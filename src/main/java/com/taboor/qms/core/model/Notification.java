package com.taboor.qms.core.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.time.OffsetDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.taboor.qms.core.utils.NotificationStatus;

@Entity
@Table(name = "tab_notifications")
public class Notification implements Serializable, Comparable<Notification> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long notificationId;
	private String title;
	private String message;
	private OffsetDateTime createdAt;
	private Boolean readStatus;
	private User user;
	private int notificationType;
	private NotificationStatus notificationStatus;
	private User userSentBy;
	private String metaData;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "notification_id", unique = true, nullable = false)
	public Long getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(Long notificationId) {
		this.notificationId = notificationId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public OffsetDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(OffsetDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public Boolean getReadStatus() {
		return readStatus;
	}

	public void setReadStatus(Boolean readStatus) {
		this.readStatus = readStatus;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "user_id", nullable = true)
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getMetaData() {
		return metaData;
	}

	public void setMetaData(String metaData) {
		this.metaData = metaData;
	}

	@Column(columnDefinition = "int default 0")
	public int getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(int notificationType) {
		this.notificationType = notificationType;
	}

	@Enumerated(EnumType.STRING)
	public NotificationStatus getNotificationStatus() {
		return notificationStatus;
	}

	public void setNotificationStatus(NotificationStatus notificationStatus) {
		this.notificationStatus = notificationStatus;
	}

	// On Delete Set Null DONE in "DbConnector -> data.sql"
	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "sent_by_user_id")
	public User getUserSentBy() {
		return userSentBy;
	}

	public void setUserSentBy(User userSentBy) {
		this.userSentBy = userSentBy;
	}

	@Override
	public int compareTo(Notification notif) {
		if (getCreatedAt() == null | notif.getCreatedAt() == null) {
			return 0;
		}
		return getCreatedAt().compareTo(notif.getCreatedAt());
	}

	public Notification() {
		super();
	}

	public Notification(Notification notification) {
		super();
		this.title = notification.title;
		this.message = notification.message;
		this.createdAt = notification.createdAt;
		this.readStatus = notification.readStatus;
		this.user = notification.user;
		this.notificationType = notification.notificationType;
		this.notificationStatus = notification.notificationStatus;
		this.userSentBy = notification.userSentBy;
		this.metaData = notification.metaData;
	}

}
