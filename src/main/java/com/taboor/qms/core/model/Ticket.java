package com.taboor.qms.core.model;
// Generated Mar 24, 2020 1:07:58 PM by Hibernate Tools 5.4.7.Final

import java.time.LocalTime;
import java.time.OffsetDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

/**
 * TabTicket generated by hbm2java
 */
@Entity
@Table(name = "tab_ticket")
public class Ticket implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long ticketId;
	private String ticketNumber;
	private int ticketType;
	private OffsetDateTime createdTime;
	private OffsetDateTime positionTime;
	private OffsetDateTime agentCallTime;
	private int ticketStatus;
	private LocalTime waitingTime;
	private String counterNumber;

	private Branch branch;
	private ServiceTAB service;
	private int transferedStatus;
	private BranchCounter transferedCounter;
	private Boolean stepOut;
	private int stepOutPosition;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ticket_id", unique = true, nullable = false)
	public Long getTicketId() {
		return ticketId;
	}

	public void setTicketId(Long ticketId) {
		this.ticketId = ticketId;
	}

	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public int getTicketType() {
		return ticketType;
	}

	public void setTicketType(int ticketType) {
		this.ticketType = ticketType;
	}

	public OffsetDateTime getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(OffsetDateTime createdTime) {
		this.createdTime = createdTime;
	}

	public OffsetDateTime getPositionTime() {
		return positionTime;
	}

	public void setPositionTime(OffsetDateTime positionTime) {
		this.positionTime = positionTime;
	}

	public OffsetDateTime getAgentCallTime() {
		return agentCallTime;
	}

	public void setAgentCallTime(OffsetDateTime agentCallTime) {
		this.agentCallTime = agentCallTime;
	}

	public int getTicketStatus() {
		return ticketStatus;
	}

	public void setTicketStatus(int ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	@Transient
	public LocalTime getWaitingTime() {
		return waitingTime;
	}

	public void setWaitingTime(LocalTime waitingTime) {
		this.waitingTime = waitingTime;
	}

	public String getCounterNumber() {
		return counterNumber;
	}

	public void setCounterNumber(String counterNumber) {
		this.counterNumber = counterNumber;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "branch_id", nullable = true)
	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "service_id", nullable = false)
	public ServiceTAB getService() {
		return service;
	}

	public void setService(ServiceTAB service) {
		this.service = service;
	}

	public int getTransferedStatus() {
		return transferedStatus;
	}

	public void setTransferedStatus(int transferedStatus) {
		this.transferedStatus = transferedStatus;
	}

	@OneToOne(fetch = FetchType.EAGER)
	@OnDelete(action = OnDeleteAction.NO_ACTION)
	// On Delete Set Null DONE in "DbConnector -> data.sql"
	@JoinColumn(name = "branch_counter_id", nullable = true)
	public BranchCounter getTransferedCounter() {
		return transferedCounter;
	}

	public void setTransferedCounter(BranchCounter transferedCounter) {
		this.transferedCounter = transferedCounter;
	}

	public Boolean getStepOut() {
		return stepOut;
	}

	public void setStepOut(Boolean stepOut) {
		this.stepOut = stepOut;
	}

	@Column(columnDefinition = "int default 0")
	public int getStepOutPosition() {
		return stepOutPosition;
	}

	public void setStepOutPosition(int stepOutPosition) {
		this.stepOutPosition = stepOutPosition;
	}
}
