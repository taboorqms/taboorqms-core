package com.taboor.qms.core.model;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tab_payment_method")
public class PaymentMethod implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long paymentMethodId;
	private String paymentMethodName;
	private String paymentMethodDescription;
	private String paymentMethodImageUrl;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "payment_method_id", unique = true, nullable = false)
	public Long getPaymentMethodId() {
		return paymentMethodId;
	}

	public void setPaymentMethodId(Long paymentMethodId) {
		this.paymentMethodId = paymentMethodId;
	}

	@Column(unique = true)
	public String getPaymentMethodName() {
		return paymentMethodName;
	}

	public void setPaymentMethodName(String paymentMethodName) {
		this.paymentMethodName = paymentMethodName;
	}

	public String getPaymentMethodDescription() {
		return paymentMethodDescription;
	}

	public void setPaymentMethodDescription(String paymentMethodDescription) {
		this.paymentMethodDescription = paymentMethodDescription;
	}

	public String getPaymentMethodImageUrl() {
		return paymentMethodImageUrl;
	}

	public void setPaymentMethodImageUrl(String paymentMethodImageUrl) {
		this.paymentMethodImageUrl = paymentMethodImageUrl;
	}
}
