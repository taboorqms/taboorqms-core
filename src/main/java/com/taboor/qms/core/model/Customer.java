package com.taboor.qms.core.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "tab_customer")
public class Customer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long customerId;
//	private String guid;
//	private Boolean emailVerified;
//	private String pictureUrl;
	private User user;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "customer_id", unique = true, nullable = false)
	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

//	public String getGuid() {
//		return guid;
//	}
//
//	public void setGuid(String guid) {
//		this.guid = guid;
//	}
//
//	public Boolean getEmailVerified() {
//		return emailVerified;
//	}
//
//	public void setEmailVerified(Boolean emailVerified) {
//		this.emailVerified = emailVerified;
//	}
//
//	public String getPictureUrl() {
//		return pictureUrl;
//	}
//
//	public void setPictureUrl(String pictureUrl) {
//		this.pictureUrl = pictureUrl;
//	}

	@OneToOne(fetch = FetchType.EAGER)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "user_id", nullable = false, unique = true)
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
