package com.taboor.qms.core.model;
// Generated Mar 24, 2020 1:07:58 PM by Hibernate Tools 5.4.7.Final

import com.taboor.qms.core.utils.InvoiceItemType;

public class InvoiceItem {

	private InvoiceItemType itemType;
	private String description;
	private Long paymentPlanId;
	private int quantity;
	private int timeInMonths;
	private Double price;

	public InvoiceItemType getItemType() {
		return itemType;
	}

	public void setItemType(InvoiceItemType itemType) {
		this.itemType = itemType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getPaymentPlanId() {
		return paymentPlanId;
	}

	public void setPaymentPlanId(Long paymentPlanId) {
		this.paymentPlanId = paymentPlanId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getTimeInMonths() {
		return timeInMonths;
	}

	public void setTimeInMonths(int timeInMonths) {
		this.timeInMonths = timeInMonths;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

}
