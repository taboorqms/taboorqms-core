package com.taboor.qms.core.model;
// Generated Oct 7, 2019 12:09:29 AM by Hibernate Tools 5.2.12.Final

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "tab_user_configuration")
public class UserConfiguration implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long configurationId;

	private Boolean inAppNotificationAllowed;
	private Boolean smsAllowed;
	private Boolean emailAllowed;

	private User user;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "configuration_id", unique = true, nullable = false)
	public Long getConfigurationId() {
		return configurationId;
	}

	public void setConfigurationId(Long configurationId) {
		this.configurationId = configurationId;
	}

	public Boolean getInAppNotificationAllowed() {
		return inAppNotificationAllowed;
	}

	public void setInAppNotificationAllowed(Boolean inAppNotificationAllowed) {
		this.inAppNotificationAllowed = inAppNotificationAllowed;
	}

	public Boolean getSmsAllowed() {
		return smsAllowed;
	}

	public void setSmsAllowed(Boolean smsAllowed) {
		this.smsAllowed = smsAllowed;
	}

	public Boolean getEmailAllowed() {
		return emailAllowed;
	}

	public void setEmailAllowed(Boolean emailAllowed) {
		this.emailAllowed = emailAllowed;
	}

	@OneToOne(fetch = FetchType.EAGER)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "user_id", nullable = false, unique = true)
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
