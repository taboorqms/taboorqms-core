package com.taboor.qms.core.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.time.OffsetDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "tab_user_verification")
public class UserVerification implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long userVerificationId;
	private String verificationCode;
	private OffsetDateTime createdTime;
	private OffsetDateTime expiryTime;
	private OffsetDateTime verifiedTime;
	private int verificationMode;
	private int verificationStatus;
	private String verificationMedium;

	private User user;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "user_verification_id", unique = true, nullable = false)
	public Long getUserVerificationId() {
		return userVerificationId;
	}

	public void setUserVerificationId(Long userVerificationId) {
		this.userVerificationId = userVerificationId;
	}

	public String getVerificationCode() {
		return verificationCode;
	}

	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}

	public OffsetDateTime getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(OffsetDateTime createdTime) {
		this.createdTime = createdTime;
	}

	public OffsetDateTime getExpiryTime() {
		return expiryTime;
	}

	public void setExpiryTime(OffsetDateTime expiryTime) {
		this.expiryTime = expiryTime;
	}

	public OffsetDateTime getVerifiedTime() {
		return verifiedTime;
	}

	public void setVerifiedTime(OffsetDateTime verifiedTime) {
		this.verifiedTime = verifiedTime;
	}

	public int getVerificationMode() {
		return verificationMode;
	}

	public void setVerificationMode(int verificationMode) {
		this.verificationMode = verificationMode;
	}

	public int getVerificationStatus() {
		return verificationStatus;
	}

	public void setVerificationStatus(int verificationStatus) {
		this.verificationStatus = verificationStatus;
	}

	public String getVerificationMedium() {
		return verificationMedium;
	}

	public void setVerificationMedium(String verificationMedium) {
		this.verificationMedium = verificationMedium;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "user_id", nullable = true)
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
