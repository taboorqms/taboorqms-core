package com.taboor.qms.core.model;
// Generated Mar 24, 2020 1:07:58 PM by Hibernate Tools 5.4.7.Final

import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

/**
 * TabQueue generated by hbm2java
 */
@Entity
@Table(name = "tab_queue")
public class Queue implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long queueId;
	private String queueNumber;
	private LocalTime averageServiceTime;
	private int averageServiceTimeCount;
	private LocalTime averageWaitTime;
	private int averageWaitTimeCount;
	private Branch branch;
	private ServiceTAB service;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "queue_id", unique = true, nullable = false)
	public Long getQueueId() {
		return queueId;
	}

	public void setQueueId(Long queueId) {
		this.queueId = queueId;
	}

	public String getQueueNumber() {
		return queueNumber;
	}

	public void setQueueNumber(String queueNumber) {
		this.queueNumber = queueNumber;
	}

	public LocalTime getAverageServiceTime() {
		return averageServiceTime;
	}

	public void setAverageServiceTime(LocalTime averageServiceTime) {
		this.averageServiceTime = averageServiceTime;
	}

	@Column(columnDefinition = "int default 0")
	public int getAverageServiceTimeCount() {
		return averageServiceTimeCount;
	}

	public void setAverageServiceTimeCount(int averageServiceTimeCount) {
		this.averageServiceTimeCount = averageServiceTimeCount;
	}

	public LocalTime getAverageWaitTime() {
		return averageWaitTime;
	}

	public void setAverageWaitTime(LocalTime averageWaitTime) {
		this.averageWaitTime = averageWaitTime;
	}

	@Column(columnDefinition = "int default 0")
	public int getAverageWaitTimeCount() {
		return averageWaitTimeCount;
	}

	public void setAverageWaitTimeCount(int averageWaitTimeCount) {
		this.averageWaitTimeCount = averageWaitTimeCount;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "branch_id", nullable = false)
	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "service_id", nullable = false)
	public ServiceTAB getService() {
		return service;
	}

	public void setService(ServiceTAB service) {
		this.service = service;
	}

}
