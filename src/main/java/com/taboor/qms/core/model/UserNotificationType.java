package com.taboor.qms.core.model;
// Generated Oct 7, 2019 12:09:29 AM by Hibernate Tools 5.2.12.Final

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "tab_user_notification_type")
public class UserNotificationType implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long notificationTypeId;

	private Boolean newOrUpdateUser;
	private Boolean newOrUpdateAgent;
	private Boolean newOrUpdateBranch;
	private Boolean updateSupportTicket;
	private Boolean customerFeedback1Star;
	private Boolean customerFeedback2Star;
	private Boolean customerFeedback3Star;
	private Boolean customerFeedback4Star;
	private Boolean customerFeedback5Star;

	private Boolean newOrUpdateServiceCenter;

	private User user;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "notification_type_id", unique = true, nullable = false)
	public Long getNotificationTypeId() {
		return notificationTypeId;
	}

	public void setNotificationTypeId(Long notificationTypeId) {
		this.notificationTypeId = notificationTypeId;
	}

	public Boolean getNewOrUpdateUser() {
		return newOrUpdateUser;
	}

	public void setNewOrUpdateUser(Boolean newOrUpdateUser) {
		this.newOrUpdateUser = newOrUpdateUser;
	}

	public Boolean getNewOrUpdateAgent() {
		return newOrUpdateAgent;
	}

	public void setNewOrUpdateAgent(Boolean newOrUpdateAgent) {
		this.newOrUpdateAgent = newOrUpdateAgent;
	}

	public Boolean getNewOrUpdateBranch() {
		return newOrUpdateBranch;
	}

	public void setNewOrUpdateBranch(Boolean newOrUpdateBranch) {
		this.newOrUpdateBranch = newOrUpdateBranch;
	}

	public Boolean getUpdateSupportTicket() {
		return updateSupportTicket;
	}

	public void setUpdateSupportTicket(Boolean updateSupportTicket) {
		this.updateSupportTicket = updateSupportTicket;
	}

	public Boolean getCustomerFeedback1Star() {
		return customerFeedback1Star;
	}

	public void setCustomerFeedback1Star(Boolean customerFeedback1Star) {
		this.customerFeedback1Star = customerFeedback1Star;
	}

	public Boolean getCustomerFeedback2Star() {
		return customerFeedback2Star;
	}

	public void setCustomerFeedback2Star(Boolean customerFeedback2Star) {
		this.customerFeedback2Star = customerFeedback2Star;
	}

	public Boolean getCustomerFeedback3Star() {
		return customerFeedback3Star;
	}

	public void setCustomerFeedback3Star(Boolean customerFeedback3Star) {
		this.customerFeedback3Star = customerFeedback3Star;
	}

	public Boolean getCustomerFeedback4Star() {
		return customerFeedback4Star;
	}

	public void setCustomerFeedback4Star(Boolean customerFeedback4Star) {
		this.customerFeedback4Star = customerFeedback4Star;
	}

	public Boolean getCustomerFeedback5Star() {
		return customerFeedback5Star;
	}

	public void setCustomerFeedback5Star(Boolean customerFeedback5Star) {
		this.customerFeedback5Star = customerFeedback5Star;
	}

	public Boolean getNewOrUpdateServiceCenter() {
		return newOrUpdateServiceCenter;
	}

	public void setNewOrUpdateServiceCenter(Boolean newOrUpdateServiceCenter) {
		this.newOrUpdateServiceCenter = newOrUpdateServiceCenter;
	}

	@OneToOne(fetch = FetchType.EAGER)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "user_id", nullable = false, unique = true)
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
