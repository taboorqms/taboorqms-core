package com.taboor.qms.core.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tab_user_manual")
public class UserManual implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long userManualId;
	private String name;
	private String path;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "user_manual_id", unique = true, nullable = false)
	public Long getUserManualId() {
		return userManualId;
	}

	public void setUserManualId(Long userManualId) {
		this.userManualId = userManualId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

}
