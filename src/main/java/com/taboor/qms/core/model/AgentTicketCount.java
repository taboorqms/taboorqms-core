package com.taboor.qms.core.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "tab_agent_ticket_count")
public class AgentTicketCount implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Long agentTicketCountId;
	
	private int totalTicketServed;
	private int totalTicketCancelled;
	private int totalTicketTransferred;
	private int totalTicketRecalled;
	private int totalSum;

	private LocalDate servingDate;
	private User servingAgent;
	private BranchCounter servingCounter;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "agent_ticket_count_id", unique = true, nullable = false)
	public Long getAgentTicketCountId() {
		return agentTicketCountId;
	}

	public void setAgentTicketCountId(Long agentTicketCountId) {
		this.agentTicketCountId = agentTicketCountId;
	}

	
	public int getTotalTicketServed() {
		return totalTicketServed;
	}

	public void setTotalTicketServed(int totalTicketServed) {
		this.totalTicketServed = totalTicketServed;
	}

	public int getTotalTicketCancelled() {
		return totalTicketCancelled;
	}

	public void setTotalTicketCancelled(int totalTicketCancelled) {
		this.totalTicketCancelled = totalTicketCancelled;
	}

	public int getTotalTicketTransferred() {
		return totalTicketTransferred;
	}

	public void setTotalTicketTransferred(int totalTicketTransferred) {
		this.totalTicketTransferred = totalTicketTransferred;
	}

	public int getTotalTicketRecalled() {
		return totalTicketRecalled;
	}

	public void setTotalTicketRecalled(int totalTicketRecalled) {
		this.totalTicketRecalled = totalTicketRecalled;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "user_id", nullable = true)
	public User getServingAgent() {
		return servingAgent;
	}

	public void setServingAgent(User servingAgent) {
		this.servingAgent = servingAgent;
	}

	public LocalDate getServingDate() {
		return servingDate;
	}

	public void setServingDate(LocalDate servingDate) {
		this.servingDate = servingDate;
	}

	public int getTotalSum() {
		return totalSum;
	}

	public void setTotalSum(int totalSum) {
		this.totalSum = totalSum;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "counter_id", nullable = true)
	public BranchCounter getServingCounter() {
		return servingCounter;
	}

	public void setServingCounter(BranchCounter servingCounter) {
		this.servingCounter = servingCounter;
	}

}
