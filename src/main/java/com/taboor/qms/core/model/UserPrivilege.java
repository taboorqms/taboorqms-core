package com.taboor.qms.core.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.taboor.qms.core.utils.PrivilegeName;

@Entity
@Table(name = "tab_user_privilege")
public class UserPrivilege implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long privilegeId;
	private PrivilegeName privilegeName;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "privilege_id", unique = true, nullable = false)
	public Long getPrivilegeId() {
		return privilegeId;
	}

	public void setPrivilegeId(Long privilegeId) {
		this.privilegeId = privilegeId;
	}

	@Enumerated(EnumType.STRING)
	@Column(unique = true)
	public PrivilegeName getPrivilegeName() {
		return privilegeName;
	}

	public void setPrivilegeName(PrivilegeName privilegeName) {
		this.privilegeName = privilegeName;
	}

}
