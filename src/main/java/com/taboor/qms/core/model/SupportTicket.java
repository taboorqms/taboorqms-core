package com.taboor.qms.core.model;
// Generated Mar 24, 2020 1:07:58 PM by Hibernate Tools 5.4.7.Final

import java.time.OffsetDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.taboor.qms.core.utils.StatusHistoryObject;
import com.taboor.qms.core.utils.SupportTicketStatus;

/**
 * TabBranch generated by hbm2java
 */
@Entity
@Table(name = "tab_support_ticket")
public class SupportTicket implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long supportTicketId;
	private String subject;
	private String description;
	private OffsetDateTime createdOn;
	private OffsetDateTime updatedOn;
	private SupportTicketStatus supportTicketStatus;
	private List<String> filePathList;

	private ServiceCenterEmployee empCreatedBy;
	private TaboorEmployee empAssignedTo;

	private String statusHistory;
	private List<StatusHistoryObject> statusHistoryList;
	private String statusReason;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "support_ticket_id", unique = true, nullable = false)
	public Long getSupportTicketId() {
		return supportTicketId;
	}

	public void setSupportTicketId(Long supportTicketId) {
		this.supportTicketId = supportTicketId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public OffsetDateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(OffsetDateTime createdOn) {
		this.createdOn = createdOn;
	}

	public OffsetDateTime getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(OffsetDateTime updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Enumerated(EnumType.STRING)
	public SupportTicketStatus getSupportTicketStatus() {
		return supportTicketStatus;
	}

	public void setSupportTicketStatus(SupportTicketStatus supportTicketStatus) {
		this.supportTicketStatus = supportTicketStatus;
	}

	@Column(length = 2000)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@ElementCollection(targetClass = String.class, fetch = FetchType.EAGER)
	@JoinColumn(nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	public List<String> getFilePathList() {
		return filePathList;
	}

	public void setFilePathList(List<String> filePathList) {
		this.filePathList = filePathList;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "emp_assigned_to", nullable = true)
	// On Delete Set Null DONE in "DbConnector -> data.sql"
	@OnDelete(action = OnDeleteAction.NO_ACTION)
	public TaboorEmployee getEmpAssignedTo() {
		return empAssignedTo;
	}

	public void setEmpAssignedTo(TaboorEmployee empAssignedTo) {
		this.empAssignedTo = empAssignedTo;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "emp_created_by", nullable = false)
	public ServiceCenterEmployee getEmpCreatedBy() {
		return empCreatedBy;
	}

	public void setEmpCreatedBy(ServiceCenterEmployee empCreatedBy) {
		this.empCreatedBy = empCreatedBy;
	}

	public String getStatusReason() {
		return statusReason;
	}

	public void setStatusReason(String statusReason) {
		this.statusReason = statusReason;
	}

	@Column(length = 3000)
	public String getStatusHistory() {
		return statusHistory;
	}

	public void setStatusHistory(String statusHistory) {
		this.statusHistory = statusHistory;
	}

	@Transient
	public List<StatusHistoryObject> getStatusHistoryList() {
		return statusHistoryList;
	}

	public void setStatusHistoryList(List<StatusHistoryObject> statusHistoryList) {
		this.statusHistoryList = statusHistoryList;
	}
}
