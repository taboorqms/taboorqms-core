package com.taboor.qms.core.model;

import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

// Generated Mar 24, 2020 1:07:58 PM by Hibernate Tools 5.4.7.Final

/**
 * TabAgentDetails generated by hbm2java
 */
@Entity
@Table(name = "tab_agent")
public class Agent implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long agentId;
	private LocalTime averageServiceTime;
	private int averageServiceTimeCount;
	private LocalTime totalWorkHours;
	private int totalTicketServed;

	private Branch branch;
	private ServiceCenterEmployee serviceCenterEmployee;
	private boolean loginStatus = false;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "agent_id", unique = true, nullable = false)
	public Long getAgentId() {
		return agentId;
	}

	public void setAgentId(Long agentId) {
		this.agentId = agentId;
	}

	public LocalTime getAverageServiceTime() {
		return averageServiceTime;
	}

	public void setAverageServiceTime(LocalTime averageServiceTime) {
		this.averageServiceTime = averageServiceTime;
	}

	@Column(columnDefinition = "int default 0")
	public int getAverageServiceTimeCount() {
		return averageServiceTimeCount;
	}

	public void setAverageServiceTimeCount(int averageServiceTimeCount) {
		this.averageServiceTimeCount = averageServiceTimeCount;
	}

	public LocalTime getTotalWorkHours() {
		return totalWorkHours;
	}

	public void setTotalWorkHours(LocalTime totalWorkHours) {
		this.totalWorkHours = totalWorkHours;
	}

	public int getTotalTicketServed() {
		return totalTicketServed;
	}

	public void setTotalTicketServed(int totalTicketServed) {
		this.totalTicketServed = totalTicketServed;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "employee_id", nullable = false)
	public ServiceCenterEmployee getServiceCenterEmployee() {
		return serviceCenterEmployee;
	}

	public void setServiceCenterEmployee(ServiceCenterEmployee serviceCenterEmployee) {
		this.serviceCenterEmployee = serviceCenterEmployee;
	}

	// On Delete Set Null DONE in "DbConnector -> data.sql"
	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@OnDelete(action = OnDeleteAction.NO_ACTION)
	@JoinColumn(name = "branch_id")
	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public boolean isLoginStatus() {
		return loginStatus;
	}

	public void setLoginStatus(boolean loginStatus) {
		this.loginStatus = loginStatus;
	}

}
