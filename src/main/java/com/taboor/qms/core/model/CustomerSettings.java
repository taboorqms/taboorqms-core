package com.taboor.qms.core.model;
// Generated Oct 7, 2019 12:09:29 AM by Hibernate Tools 5.2.12.Final

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "tab_customer_settings")
public class CustomerSettings implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long settingId;
	private Boolean privacyName;
	private Boolean privacyPhoneNumber;
	private Boolean privacyEmail;
	
	private Boolean notificationSMS;
	private Boolean notificationEmail;
	private Boolean notificationPushNotification;
	
	private Boolean showBranchesDistance;
	private Boolean showBranchesFastestQueue;
	
	private Customer customer;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "setting_id", unique = true, nullable = false)
	public Long getSettingId() {
		return settingId;
	}

	public void setSettingId(Long settingId) {
		this.settingId = settingId;
	}

	public Boolean getPrivacyName() {
		return privacyName;
	}

	public void setPrivacyName(Boolean privacyName) {
		this.privacyName = privacyName;
	}

	public Boolean getPrivacyPhoneNumber() {
		return privacyPhoneNumber;
	}

	public void setPrivacyPhoneNumber(Boolean privacyPhoneNumber) {
		this.privacyPhoneNumber = privacyPhoneNumber;
	}

	public Boolean getPrivacyEmail() {
		return privacyEmail;
	}

	public void setPrivacyEmail(Boolean privacyEmail) {
		this.privacyEmail = privacyEmail;
	}

	public Boolean getNotificationSMS() {
		return notificationSMS;
	}

	public void setNotificationSMS(Boolean notificationSMS) {
		this.notificationSMS = notificationSMS;
	}

	public Boolean getNotificationEmail() {
		return notificationEmail;
	}

	public void setNotificationEmail(Boolean notificationEmail) {
		this.notificationEmail = notificationEmail;
	}

	public Boolean getNotificationPushNotification() {
		return notificationPushNotification;
	}

	public void setNotificationPushNotification(Boolean notificationPushNotification) {
		this.notificationPushNotification = notificationPushNotification;
	}

	public Boolean getShowBranchesDistance() {
		return showBranchesDistance;
	}

	public void setShowBranchesDistance(Boolean showBranchesDistance) {
		this.showBranchesDistance = showBranchesDistance;
	}

	public Boolean getShowBranchesFastestQueue() {
		return showBranchesFastestQueue;
	}

	public void setShowBranchesFastestQueue(Boolean showBranchesFastestQueue) {
		this.showBranchesFastestQueue = showBranchesFastestQueue;
	}

	@OneToOne(fetch = FetchType.EAGER)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "customer_id", nullable = false)
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}	
	
}
