package com.taboor.qms.core.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tab_user_faq_type")
public class UserFAQType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long userFaqTypeId;
	private String userFaqTypeName;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "user_faq_type_id", unique = true, nullable = false)
	public Long getUserFaqTypeId() {
		return userFaqTypeId;
	}

	public void setUserFaqTypeId(Long userFaqTypeId) {
		this.userFaqTypeId = userFaqTypeId;
	}

	@Column(unique = true)
	public String getUserFaqTypeName() {
		return userFaqTypeName;
	}

	public void setUserFaqTypeName(String userFaqTypeName) {
		this.userFaqTypeName = userFaqTypeName;
	}

}
