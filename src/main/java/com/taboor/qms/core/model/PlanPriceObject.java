package com.taboor.qms.core.model;

import com.taboor.qms.core.utils.Currency;

public class PlanPriceObject {

	private Double perUserPrice;
	private Double perSMSPrice;
	private Double priceMonthly;
	private Double priceAnnually;
	private Currency currency;

	public Double getPerUserPrice() {
		return perUserPrice;
	}

	public void setPerUserPrice(Double perUserPrice) {
		this.perUserPrice = perUserPrice;
	}

	public Double getPerSMSPrice() {
		return perSMSPrice;
	}

	public void setPerSMSPrice(Double perSMSPrice) {
		this.perSMSPrice = perSMSPrice;
	}

	public Double getPriceMonthly() {
		return priceMonthly;
	}

	public void setPriceMonthly(Double priceMonthly) {
		this.priceMonthly = priceMonthly;
	}

	public Double getPriceAnnually() {
		return priceAnnually;
	}

	public void setPriceAnnually(Double priceAnnually) {
		this.priceAnnually = priceAnnually;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

}
