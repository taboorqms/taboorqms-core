package com.taboor.qms.core.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "tab_user_faq")
public class UserFAQ implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long userFaqId;
	private String question;
	private String answer;

	private UserFAQType userFAQType;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "user_faq_id", unique = true, nullable = false)
	public Long getUserFaqId() {
		return userFaqId;
	}

	public void setUserFaqId(Long userFaqId) {
		this.userFaqId = userFaqId;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	@Column(length = 2000)
	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "user_faq_type_id", nullable = false)
	public UserFAQType getUserFAQType() {
		return userFAQType;
	}

	public void setUserFAQType(UserFAQType userFAQType) {
		this.userFAQType = userFAQType;
	}

}
