//package com.taboor.qms.core.model;
//
//import static javax.persistence.GenerationType.IDENTITY;
//
//import java.io.Serializable;
//import java.time.OffsetDateTime;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.FetchType;
//import javax.persistence.GeneratedValue;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
//import javax.persistence.Table;
//
//import org.hibernate.annotations.OnDelete;
//import org.hibernate.annotations.OnDeleteAction;
//
//@Entity
//@Table(name = "tab_user_logs")
//public class UserLog implements Serializable {
//
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 1L;
//	private Long logId;
//	private String clientTransactionId;
//	private String subject;
//	private OffsetDateTime createdTime;
//	private String url;
//	private String method;
//	private String ipAddress;
//	private String userAgent;
//	private String requestData;
//	private String responseData;
//
//	private User user;
//
//	@Id
//	@GeneratedValue(strategy = IDENTITY)
//	@Column(name = "log_id", unique = true, nullable = false)
//	public Long getLogId() {
//		return logId;
//	}
//
//	public void setLogId(Long logId) {
//		this.logId = logId;
//	}
//
//	public String getClientTransactionId() {
//		return clientTransactionId;
//	}
//
//	public void setClientTransactionId(String clientTransactionId) {
//		this.clientTransactionId = clientTransactionId;
//	}
//
//	public String getSubject() {
//		return subject;
//	}
//
//	public void setSubject(String subject) {
//		this.subject = subject;
//	}
//
//	public OffsetDateTime getCreatedTime() {
//		return createdTime;
//	}
//
//	public void setCreatedTime(OffsetDateTime createdTime) {
//		this.createdTime = createdTime;
//	}
//
//	public String getUrl() {
//		return url;
//	}
//
//	public void setUrl(String url) {
//		this.url = url;
//	}
//
//	public String getMethod() {
//		return method;
//	}
//
//	public void setMethod(String method) {
//		this.method = method;
//	}
//
//	public String getIpAddress() {
//		return ipAddress;
//	}
//
//	public void setIpAddress(String ipAddress) {
//		this.ipAddress = ipAddress;
//	}
//
//	public String getUserAgent() {
//		return userAgent;
//	}
//
//	public void setUserAgent(String userAgent) {
//		this.userAgent = userAgent;
//	}
//
//	public String getRequestData() {
//		return requestData;
//	}
//
//	public void setRequestData(String requestData) {
//		this.requestData = requestData;
//	}
//
//	public String getResponseData() {
//		return responseData;
//	}
//
//	public void setResponseData(String responseData) {
//		this.responseData = responseData;
//	}
//
//	@ManyToOne(fetch = FetchType.EAGER)
//	@OnDelete(action = OnDeleteAction.CASCADE)
//	@JoinColumn(name = "user_id", nullable = false)
//	public User getUser() {
//		return user;
//	}
//
//	public void setUser(User user) {
//		this.user = user;
//	}
//}
