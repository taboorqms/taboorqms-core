package com.taboor.qms.core.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "tab_ticket_feedback")
public class TicketFeedback implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long ticketFeedbackId;
	private Double rating;
	private int servicePoints;
	private String comment;
	private User user;
	private Ticket ticket;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "user_feedback_id", unique = true, nullable = false)
	public Long getTicketFeedbackId() {
		return ticketFeedbackId;
	}

	public void setTicketFeedbackId(Long ticketFeedbackId) {
		this.ticketFeedbackId = ticketFeedbackId;
	}

	public Double getRating() {
		return rating;
	}

	public void setRating(Double rating) {
		this.rating = rating;
	}

	public int getServicePoints() {
		return servicePoints;
	}

	public void setServicePoints(int servicePoints) {
		this.servicePoints = servicePoints;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "user_id", nullable = true)
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "ticket_id", nullable = false)
	public Ticket getTicket() {
		return ticket;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}
}
