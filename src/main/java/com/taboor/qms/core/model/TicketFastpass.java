package com.taboor.qms.core.model;
// Generated Mar 24, 2020 1:07:58 PM by Hibernate Tools 5.4.7.Final

import static javax.persistence.GenerationType.IDENTITY;

import java.time.OffsetDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

/**
 * TabTicketFastpass generated by hbm2java
 */
@Entity
@Table(name = "tab_ticket_fastpass")
public class TicketFastpass implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long ticketFastpassId;
	private OffsetDateTime applyTime;
	private Double charges;
	private int status;

	private Ticket ticket;
	private UserPaymentTransaction userPaymentTransaction;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ticket_fastpass_id", unique = true, nullable = false)
	public long getTicketFastpassId() {
		return ticketFastpassId;
	}

	public void setTicketFastpassId(long ticketFastpassId) {
		this.ticketFastpassId = ticketFastpassId;
	}

	public OffsetDateTime getApplyTime() {
		return applyTime;
	}

	public void setApplyTime(OffsetDateTime applyTime) {
		this.applyTime = applyTime;
	}

	public Double getCharges() {
		return charges;
	}

	public void setCharges(Double charges) {
		this.charges = charges;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@OneToOne(fetch = FetchType.EAGER)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "ticket_id", nullable = true, unique = true)
	public Ticket getTicket() {
		return ticket;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@OnDelete(action = OnDeleteAction.NO_ACTION)
	// On Delete Set Null DONE in "DbConnector -> data.sql"
	@JoinColumn(name = "payment_transaction_id", nullable = true)
	public UserPaymentTransaction getUserPaymentTransaction() {
		return userPaymentTransaction;
	}

	public void setUserPaymentTransaction(UserPaymentTransaction userPaymentTransaction) {
		this.userPaymentTransaction = userPaymentTransaction;
	}

}
