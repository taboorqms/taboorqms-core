package com.taboor.qms.core.payload;

import java.time.LocalDate;

public class CheckAgentTicketCountPayload {

	private LocalDate servingDate;
	private Long userId;
	private Long branchCounterId;

	public LocalDate getServingDate() {
		return servingDate;
	}

	public void setServingDate(LocalDate servingDate) {
		this.servingDate = servingDate;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getBranchCounterId() {
		return branchCounterId;
	}

	public void setBranchCounterId(Long branchCounterId) {
		this.branchCounterId = branchCounterId;
	}

}
