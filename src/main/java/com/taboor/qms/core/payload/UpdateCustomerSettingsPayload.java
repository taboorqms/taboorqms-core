package com.taboor.qms.core.payload;

public class UpdateCustomerSettingsPayload {

	private Boolean privacyName;
	private Boolean privacyPhoneNumber;
	private Boolean privacyEmail;

	private Boolean notificationSMS;
	private Boolean notificationEmail;
	private Boolean notificationPushNotification;

	private Boolean showBranchesDistance;
	private Boolean showBranchesFastestQueue;

	public Boolean getPrivacyName() {
		return privacyName;
	}

	public void setPrivacyName(Boolean privacyName) {
		this.privacyName = privacyName;
	}

	public Boolean getPrivacyPhoneNumber() {
		return privacyPhoneNumber;
	}

	public void setPrivacyPhoneNumber(Boolean privacyPhoneNumber) {
		this.privacyPhoneNumber = privacyPhoneNumber;
	}

	public Boolean getPrivacyEmail() {
		return privacyEmail;
	}

	public void setPrivacyEmail(Boolean privacyEmail) {
		this.privacyEmail = privacyEmail;
	}

	public Boolean getNotificationSMS() {
		return notificationSMS;
	}

	public void setNotificationSMS(Boolean notificationSMS) {
		this.notificationSMS = notificationSMS;
	}

	public Boolean getNotificationEmail() {
		return notificationEmail;
	}

	public void setNotificationEmail(Boolean notificationEmail) {
		this.notificationEmail = notificationEmail;
	}

	public Boolean getNotificationPushNotification() {
		return notificationPushNotification;
	}

	public void setNotificationPushNotification(Boolean notificationPushNotification) {
		this.notificationPushNotification = notificationPushNotification;
	}

	public Boolean getShowBranchesDistance() {
		return showBranchesDistance;
	}

	public void setShowBranchesDistance(Boolean showBranchesDistance) {
		this.showBranchesDistance = showBranchesDistance;
	}

	public Boolean getShowBranchesFastestQueue() {
		return showBranchesFastestQueue;
	}

	public void setShowBranchesFastestQueue(Boolean showBranchesFastestQueue) {
		this.showBranchesFastestQueue = showBranchesFastestQueue;
	}
}
