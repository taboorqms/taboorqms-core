package com.taboor.qms.core.payload;

public class AddTicketFeedbackCounterPayload {

	private String servicePoints;
	private String ticketNumber;

	public String getServicePoints() {
		return servicePoints;
	}

	public void setServicePoints(String servicePoints) {
		this.servicePoints = servicePoints;
	}

	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

}
