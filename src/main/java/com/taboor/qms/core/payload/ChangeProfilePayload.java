package com.taboor.qms.core.payload;

public class ChangeProfilePayload {

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
