package com.taboor.qms.core.payload;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class AddUserBankCardPayload extends BasePayload {

	private String cardType;
	private String cardNumber;
	private String cardTitle;
	private String expiryDate;
	private String cardCvv;
	private long paymentMethodId;

	private long userBankCardId;
	private long serviceCenterId;

	@NotNull
	@NotEmpty
	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	@NotNull
	@NotEmpty
	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	@NotNull
	@NotEmpty
	public String getCardTitle() {
		return cardTitle;
	}

	public void setCardTitle(String cardTitle) {
		this.cardTitle = cardTitle;
	}	

	@NotNull
	@NotEmpty
	public String getCardCvv() {
		return cardCvv;
	}

	public void setCardCvv(String cardCvv) {
		this.cardCvv = cardCvv;
	}

	@NotBlank
	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public long getPaymentMethodId() {
		return paymentMethodId;
	}

	public void setPaymentMethodId(long paymentMethodId) {
		this.paymentMethodId = paymentMethodId;
	}

	public long getUserBankCardId() {
		return userBankCardId;
	}

	public void setUserBankCardId(long userBankCardId) {
		this.userBankCardId = userBankCardId;
	}

	public long getServiceCenterId() {
		return serviceCenterId;
	}

	public void setServiceCenterId(long serviceCenterId) {
		this.serviceCenterId = serviceCenterId;
	}

}
