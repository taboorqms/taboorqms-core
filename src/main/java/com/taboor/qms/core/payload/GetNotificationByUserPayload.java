package com.taboor.qms.core.payload;

import com.taboor.qms.core.model.User;

public class GetNotificationByUserPayload {

	private int pageNo;
	private int limit;
	private User user;

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
