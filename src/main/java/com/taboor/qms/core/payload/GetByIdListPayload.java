package com.taboor.qms.core.payload;

import java.util.List;

public class GetByIdListPayload {

	private List<Long> ids;

	public List<Long> getIds() {
		return ids;
	}

	public void setIds(List<Long> ids) {
		this.ids = ids;
	}

	public GetByIdListPayload() {
		// TODO Auto-generated constructor stub
	}

}
