package com.taboor.qms.core.payload;

import java.time.OffsetDateTime;

import com.taboor.qms.core.model.UserBankCard;

public class CreateUserPaymentTransactionPayload {

	private OffsetDateTime expiryTime;
	private Integer transactionType;
	private Double amount;
	
	private UserBankCard userBankCard;
	private Long userBankCardId;

	public OffsetDateTime getExpiryTime() {
		return expiryTime;
	}

	public void setExpiryTime(OffsetDateTime expiryTime) {
		this.expiryTime = expiryTime;
	}

	public Integer getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(Integer transactionType) {
		this.transactionType = transactionType;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public UserBankCard getUserBankCard() {
		return userBankCard;
	}

	public void setUserBankCard(UserBankCard userBankCard) {
		this.userBankCard = userBankCard;
	}

	public Long getUserBankCardId() {
		return userBankCardId;
	}

	public void setUserBankCardId(Long userBankCardId) {
		this.userBankCardId = userBankCardId;
	}

}
