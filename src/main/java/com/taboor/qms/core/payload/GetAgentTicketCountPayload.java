package com.taboor.qms.core.payload;

import java.time.LocalDate;

public class GetAgentTicketCountPayload {

	private LocalDate servingDate;
	private Long branchCounterId;

	public LocalDate getServingDate() {
		return servingDate;
	}

	public void setServingDate(LocalDate servingDate) {
		this.servingDate = servingDate;
	}

	public Long getBranchCounterId() {
		return branchCounterId;
	}

	public void setBranchCounterId(Long branchCounterId) {
		this.branchCounterId = branchCounterId;
	}

}
