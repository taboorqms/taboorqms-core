package com.taboor.qms.core.payload;

public class AddTicketFeedbackPayload {

	private Double rating;
	private String servicePoints;
	private String comment;
	private Long ticketId;

	public Double getRating() {
		return rating;
	}

	public void setRating(Double rating) {
		this.rating = rating;
	}

	public String getServicePoints() {
		return servicePoints;
	}

	public void setServicePoints(String servicePoints) {
		this.servicePoints = servicePoints;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Long getTicketId() {
		return ticketId;
	}

	public void setTicketId(Long ticketId) {
		this.ticketId = ticketId;
	}
}
