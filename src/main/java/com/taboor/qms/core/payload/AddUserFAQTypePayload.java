package com.taboor.qms.core.payload;

public class AddUserFAQTypePayload {

	private String userFAQTypeName;

	public String getUserFAQTypeName() {
		return userFAQTypeName;
	}

	public void setUserFAQTypeName(String userFAQTypeName) {
		this.userFAQTypeName = userFAQTypeName;
	}

}
