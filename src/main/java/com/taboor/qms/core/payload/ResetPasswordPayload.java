package com.taboor.qms.core.payload;

public class ResetPasswordPayload extends BasePayload {

	private String email;
	private String resetPassword;

	public String getResetPassword() {
		return resetPassword;
	}

	public void setResetPassword(String resetPassword) {
		this.resetPassword = resetPassword;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
