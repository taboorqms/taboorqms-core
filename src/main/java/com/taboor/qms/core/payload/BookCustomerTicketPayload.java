package com.taboor.qms.core.payload;

public class BookCustomerTicketPayload {

	private long branchId;
	private long serviceId;

	public long getBranchId() {
		return branchId;
	}

	public void setBranchId(long branchId) {
		this.branchId = branchId;
	}

	public long getServiceId() {
		return serviceId;
	}

	public void setServiceId(long serviceId) {
		this.serviceId = serviceId;
	}

	public BookCustomerTicketPayload() {
		// TODO Auto-generated constructor stub
	}

}
