package com.taboor.qms.core.payload;

public class AddUserFAQPayload {

	private String question;
	private String answer;
	private Long faqTypeId;
	private Long userFAQId;

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public Long getFaqTypeId() {
		return faqTypeId;
	}

	public void setFaqTypeId(Long faqTypeId) {
		this.faqTypeId = faqTypeId;
	}

	public Long getUserFAQId() {
		return userFAQId;
	}

	public void setUserFAQId(Long userFAQId) {
		this.userFAQId = userFAQId;
	}

}
