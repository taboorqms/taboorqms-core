package com.taboor.qms.core.payload;

import javax.validation.constraints.NotBlank;

public class AddTaboorConfigurationPayload {

	private String configKey;
	private String configValue;
	private String configDescription;

	@NotBlank
	public String getConfigKey() {
		return configKey;
	}

	public void setConfigKey(String configKey) {
		this.configKey = configKey;
	}

	@NotBlank
	public String getConfigValue() {
		return configValue;
	}

	public void setConfigValue(String configValue) {
		this.configValue = configValue;
	}

	public String getConfigDescription() {
		return configDescription;
	}

	public void setConfigDescription(String configDescription) {
		this.configDescription = configDescription;
	}

}
