package com.taboor.qms.core.payload;

import java.time.LocalDateTime;
import java.util.List;

public class GetNearByBranchPayload {

	private double radius;	
	private double customerLat;	
	private double customerLong;
	private LocalDateTime customerDateTime;
	private boolean filtersApplied;
	private boolean statusAllBranches;
	private boolean statusOpenNow;
	private boolean sortByDistance;
	private boolean sortByServiceTime;
	private boolean sortByWaitingTime;
	private boolean searchByServiceName;
	private List<Long> serviceIds;
	private Long serviceCentreId;
	
	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public double getCustomerLat() {
		return customerLat;
	}

	public void setCustomerLat(double customerLat) {
		this.customerLat = customerLat;
	}

	public double getCustomerLong() {
		return customerLong;
	}

	public void setCustomerLong(double customerLong) {
		this.customerLong = customerLong;
	}

	public LocalDateTime getCustomerDateTime() {
		return customerDateTime;
	}

	public void setCustomerDateTime(LocalDateTime customerDateTime) {
		this.customerDateTime = customerDateTime;
	}

	public boolean isFiltersApplied() {
		return filtersApplied;
	}

	public void setFiltersApplied(boolean filtersApplied) {
		this.filtersApplied = filtersApplied;
	}

	public boolean isStatusAllBranches() {
		return statusAllBranches;
	}

	public void setStatusAllBranches(boolean statusAllBranches) {
		this.statusAllBranches = statusAllBranches;
	}

	public boolean isSortByDistance() {
		return sortByDistance;
	}

	public void setSortByDistance(boolean sortByDistance) {
		this.sortByDistance = sortByDistance;
	}

	public boolean isSortByServiceTime() {
		return sortByServiceTime;
	}

	public void setSortByServiceTime(boolean sortByServiceTime) {
		this.sortByServiceTime = sortByServiceTime;
	}

	public boolean isSortByWaitingTime() {
		return sortByWaitingTime;
	}

	public void setSortByWaitingTime(boolean sortByWaitingTime) {
		this.sortByWaitingTime = sortByWaitingTime;
	}

	public boolean isStatusOpenNow() {
		return statusOpenNow;
	}

	public void setStatusOpenNow(boolean statusOpenNow) {
		this.statusOpenNow = statusOpenNow;
	}

	public boolean isSearchByServiceName() {
		return searchByServiceName;
	}

	public void setSearchByServiceName(boolean searchByServiceName) {
		this.searchByServiceName = searchByServiceName;
	}

	public List<Long> getServiceIds() {
		return serviceIds;
	}

	public void setServiceIds(List<Long> serviceIds) {
		this.serviceIds = serviceIds;
	}
	
	public Long getServiceCentreId() {
		return serviceCentreId;
	}
	
	public void setServiceCentreId(Long serviceCentreId) {
		this.serviceCentreId = serviceCentreId;
	}
}