package com.taboor.qms.core.payload;

import java.util.Map;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PushUserNotificationPayload {

	@NotNull
	@Size(min = 1, max = 45)
	private String title;

	@NotNull
	@Size(min = 1, max = 200)
	private String description;
	private Map<String, String> metaData;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Map<String, String> getMetaData() {
		return metaData;
	}

	public void setMetaData(Map<String, String> metaData) {
		this.metaData = metaData;
	}
}
