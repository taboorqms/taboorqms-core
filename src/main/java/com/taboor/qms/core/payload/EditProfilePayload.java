package com.taboor.qms.core.payload;

public class EditProfilePayload {

	private String email;	

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
