package com.taboor.qms.core.payload;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.taboor.qms.core.model.User;

public class CreateSessionPayload {

	@NotNull
	@NotEmpty
	private String deviceToken;

	@NotNull
	@NotEmpty
	private String ipAddress;

	private User user;

	public String getDeviceToken() {
		return deviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

}
