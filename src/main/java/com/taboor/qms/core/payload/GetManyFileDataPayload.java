/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taboor.qms.core.payload;

import java.util.List;

/**
 *
 * @author Awais Waheed - Funavry Technologies
 */
public class GetManyFileDataPayload {
    
    private List<FilePathObject> filePaths;

    public List<FilePathObject> getFilePaths() {
        return filePaths;
    }

    public void setFilePaths(List<FilePathObject> filePaths) {
        this.filePaths = filePaths;
    }

    public GetManyFileDataPayload(List<FilePathObject> filePaths) {
        this.filePaths = filePaths;
    }

    public GetManyFileDataPayload() {
    }
            
    public static class FilePathObject{
        private String id;
        private String path;

        public FilePathObject(String id, String path) {
            this.id = id;
            this.path = path;
        }

        public FilePathObject() {
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }
        
    }
}
