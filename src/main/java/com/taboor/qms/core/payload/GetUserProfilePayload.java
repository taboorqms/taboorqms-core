package com.taboor.qms.core.payload;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class GetUserProfilePayload {
	
	@NotNull
	@NotEmpty
	@Email
	@Size(min = 1, max = 70)
	private String email;

	public String getEmail() {
		return email;
	}
}
