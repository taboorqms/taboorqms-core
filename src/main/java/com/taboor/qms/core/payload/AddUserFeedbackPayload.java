package com.taboor.qms.core.payload;

public class AddUserFeedbackPayload {

	private String comment;

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

}
