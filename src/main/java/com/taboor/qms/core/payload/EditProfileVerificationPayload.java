package com.taboor.qms.core.payload;

import javax.validation.constraints.Size;

public class EditProfileVerificationPayload {

	@Size(min = 5, max = 5)
	private String verificationCode;

	private int verificationMode;

	public String getVerificationCode() {
		return verificationCode;
	}

	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}

	public int getVerificationMode() {
		return verificationMode;
	}

	public void setVerificationMode(int verificationMode) {
		this.verificationMode = verificationMode;
	}

}
