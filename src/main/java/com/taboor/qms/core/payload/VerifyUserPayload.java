package com.taboor.qms.core.payload;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class VerifyUserPayload {

	@NotNull
	@Size(min = 1, max = 50)
	private String email;

	@Size(min = 5, max = 5)
	private String verificationCode;

	private int verificationMode;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getVerificationCode() {
		return verificationCode;
	}

	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}

	public int getVerificationMode() {
		return verificationMode;
	}

	public void setVerificationMode(int verificationMode) {
		this.verificationMode = verificationMode;
	}

}
