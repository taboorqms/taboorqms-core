package com.taboor.qms.core.payload;

public class StepInOutTicketPayload {

	private Long ticketId;
	
	public Long getTicketId() {
		return ticketId;
	}
	public void setTicketId(Long ticketId) {
		this.ticketId = ticketId;
	}
}
