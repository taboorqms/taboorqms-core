package com.taboor.qms.core.payload;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class RegisterCustomerPayload {

	@NotNull
	@NotEmpty
	@Size(min = 1, max = 50)
	private String name;

	@NotNull
	@NotEmpty
	@Size(min = 1, max = 50)
	private String phoneNumber;

	@NotNull
	@NotEmpty
	@Size(min = 1, max = 50)
	private String email;

	@NotNull
	@NotEmpty
	@Size(min = 1, max = 50)
	private String password;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
