package com.taboor.qms.core.payload;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

public class VerificationCodePayload {

	@NotNull
	private String email;
	@NotNull
	private int verificationMode;

	public int getVerificationMode() {
		return verificationMode;
	}

	public void setVerificationMode(int verificationMode) {
		this.verificationMode = verificationMode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
