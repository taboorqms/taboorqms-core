package com.taboor.qms.core.payload;

import com.taboor.qms.core.model.User;
import com.taboor.qms.core.model.UserRole;

public class RegisterUserPayload extends BasePayload {

	private User user;
	private String name;
	private String email;
	private String password;
	private String phoneNumber;
	private UserRole userRole;
	private Boolean assignRolePrivileges;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Boolean getAssignRolePrivileges() {
		return assignRolePrivileges;
	}

	public void setAssignRolePrivileges(Boolean assignRolePrivileges) {
		this.assignRolePrivileges = assignRolePrivileges;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public UserRole getUserRole() {
		return userRole;
	}

	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}
}
