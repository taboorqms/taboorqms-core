package com.taboor.qms.core.payload;

public class GetUserNotificationPayload {

	private int pageNo;
	private int limit;

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}
}
