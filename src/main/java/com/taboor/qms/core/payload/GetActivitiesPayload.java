package com.taboor.qms.core.payload;

import java.time.LocalDate;
import java.util.List;

public class GetActivitiesPayload {

	private LocalDate startDate;
	private LocalDate endDate;
	private List<Long> idList;

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public List<Long> getIdList() {
		return idList;
	}

	public void setIdList(List<Long> idList) {
		this.idList = idList;
	}

	public GetActivitiesPayload() {
		super();
	}

	public GetActivitiesPayload(LocalDate startDate, LocalDate endDate, List<Long> idList) {
		super();
		this.startDate = startDate;
		this.endDate = endDate;
		this.idList = idList;
	}

}
