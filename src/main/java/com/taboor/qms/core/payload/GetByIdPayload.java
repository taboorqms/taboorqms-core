package com.taboor.qms.core.payload;

public class GetByIdPayload {

	private long id;

	public long getId() {
		return id;
	}

	public GetByIdPayload() {
		super();
	}

	public void setId(long id) {
		this.id = id;
	}

	public GetByIdPayload(long id) {
		super();
		this.id = id;
	}

}
