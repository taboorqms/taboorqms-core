package com.taboor.qms.core.response;

import java.util.List;

import com.taboor.qms.core.model.Session;
import com.taboor.qms.core.model.UserPrivilege;

public class SessionAndPrivilegesResponse {

	private Session session;
	private List<UserPrivilege> privileges;

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}

	public List<UserPrivilege> getPrivileges() {
		return privileges;
	}

	public void setPrivileges(List<UserPrivilege> privileges) {
		this.privileges = privileges;
	}

	public SessionAndPrivilegesResponse() {
		super();
	}

	public SessionAndPrivilegesResponse(Session session, List<UserPrivilege> privileges) {
		super();
		this.session = session;
		this.privileges = privileges;
	}

}
