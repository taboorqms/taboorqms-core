package com.taboor.qms.core.response;

import java.util.List;

public class GetBranchAdminCounterListResponse extends GenStatusResponse {

	public GetBranchAdminCounterListResponse(List<BranchAdminCounter> counterList, int applicationStatusCode,
			String applicationStatusResponse) {
		super(applicationStatusCode, applicationStatusResponse);
		this.counterList = counterList;
	}

	public GetBranchAdminCounterListResponse() {

	}

	private Long branchId;
	private String branchName;
	private String branchNameArabic;
	private String emailAddress;
	private List<BranchAdminCounter> counterList;

	public class BranchAdminCounter {

		private Long counterId;
		private String counterNumber;

		public Long getCounterId() {
			return counterId;
		}

		public void setCounterId(Long counterId) {
			this.counterId = counterId;
		}

		public String getCounterNumber() {
			return counterNumber;
		}

		public void setCounterNumber(String counterNumber) {
			this.counterNumber = counterNumber;
		}

	}

	public Long getBranchId() {
		return branchId;
	}

	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getBranchNameArabic() {
		return branchNameArabic;
	}

	public void setBranchNameArabic(String branchNameArabic) {
		this.branchNameArabic = branchNameArabic;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public List<BranchAdminCounter> getCounterList() {
		return counterList;
	}

	public void setCounterList(List<BranchAdminCounter> counterList) {
		this.counterList = counterList;
	}

}
