package com.taboor.qms.core.response;

public class BooleanResponse extends GenStatusResponse {

	private Boolean value;

	public Boolean getValue() {
		return value;
	}

	public void setValue(Boolean value) {
		this.value = value;
	}

	public BooleanResponse() {
		// TODO Auto-generated constructor stub
	}

	public BooleanResponse(int applicationStatusCode, String applicationStatusResponse, Boolean value) {
		super(applicationStatusCode, applicationStatusResponse);
		this.value = value;
	}

}
