package com.taboor.qms.core.response;

import java.util.List;

import com.taboor.qms.core.model.Branch;

public class GetBranchListResponse extends GenStatusResponse {

	private List<Branch> branchList;

	public List<Branch> getBranchList() {
		return branchList;
	}

	public void setBranchList(List<Branch> branchList) {
		this.branchList = branchList;
	}

	public GetBranchListResponse() {
		// TODO Auto-generated constructor stub
	}

	public GetBranchListResponse(int applicationStatusCode, String applicationStatusResponse, List<Branch> branchList) {
		super(applicationStatusCode, applicationStatusResponse);
		this.branchList = branchList;
	}

}
