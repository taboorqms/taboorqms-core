package com.taboor.qms.core.response;

import java.util.List;

import com.taboor.qms.core.model.Branch;
import com.taboor.qms.core.model.ServiceTAB;

public class GetBranchServicesResponse extends GenStatusResponse {

	private Branch branch;
	private List<ServiceTAB> serviceList;

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public List<ServiceTAB> getServiceList() {
		return serviceList;
	}

	public void setServiceList(List<ServiceTAB> serviceList) {
		this.serviceList = serviceList;
	}

	public GetBranchServicesResponse() {
		// TODO Auto-generated constructor stub
	}

	public GetBranchServicesResponse(int applicationStatusCode, String applicationStatusResponse, Branch branch,
			List<ServiceTAB> serviceList) {
		super(applicationStatusCode, applicationStatusResponse);
		this.branch = branch;
		this.serviceList = serviceList;
	}

}
