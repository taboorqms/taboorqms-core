package com.taboor.qms.core.response;

import java.util.List;

import com.taboor.qms.core.model.TicketFastpass;

public class GetTicketFastpassListResponse extends GenStatusResponse {

	private List<TicketFastpass> ticketFastpassList;

	public List<TicketFastpass> getTicketFastpassList() {
		return ticketFastpassList;
	}

	public void setTicketFastpassList(List<TicketFastpass> ticketFastpassList) {
		this.ticketFastpassList = ticketFastpassList;
	}

	public GetTicketFastpassListResponse() {
		// TODO Auto-generated constructor stub
	}

	public GetTicketFastpassListResponse(int applicationStatusCode, String applicationStatusResponse,
			List<TicketFastpass> ticketFastpassList) {
		super(applicationStatusCode, applicationStatusResponse);
		this.ticketFastpassList = ticketFastpassList;
	}

}
