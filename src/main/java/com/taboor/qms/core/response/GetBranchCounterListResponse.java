package com.taboor.qms.core.response;

import java.util.List;

import com.taboor.qms.core.model.BranchCounter;
import com.taboor.qms.core.model.ServiceTAB;

public class GetBranchCounterListResponse extends GenStatusResponse {

	List<BranchCounterObject> branchCounterList;

	public List<BranchCounterObject> getBranchCounterList() {
		return branchCounterList;
	}

	public void setBranchCounterList(List<BranchCounterObject> branchCounterList) {
		this.branchCounterList = branchCounterList;
	}

	public static class BranchCounterObject {
		private BranchCounter branchCounter;
		private List<ServiceTAB> providedServices;
		private Boolean assignedToAgent;

		public BranchCounter getBranchCounter() {
			return branchCounter;
		}

		public void setBranchCounter(BranchCounter branchCounter) {
			this.branchCounter = branchCounter;
		}

		public List<ServiceTAB> getProvidedServices() {
			return providedServices;
		}

		public void setProvidedServices(List<ServiceTAB> providedServices) {
			this.providedServices = providedServices;
		}

		public Boolean getAssignedToAgent() {
			return assignedToAgent;
		}

		public void setAssignedToAgent(Boolean assignedToAgent) {
			this.assignedToAgent = assignedToAgent;
		}

	}

	public GetBranchCounterListResponse() {
		super();
	}

	public GetBranchCounterListResponse(int applicationStatusCode, String applicationStatusResponse,
			List<BranchCounterObject> branchCounterList) {
		super(applicationStatusCode, applicationStatusResponse);
		this.branchCounterList = branchCounterList;
	}

}
