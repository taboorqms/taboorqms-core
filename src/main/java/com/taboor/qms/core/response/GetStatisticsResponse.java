package com.taboor.qms.core.response;

import java.util.ArrayList;
import java.util.List;

public class GetStatisticsResponse extends GenStatusResponse {

	List<ValueObject> values = new ArrayList<ValueObject>();

	public static class ValueObject {
		private String key;
		private String value;

		public String getKey() {
			return key;
		}

		public void setKey(String key) {
			this.key = key;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		public ValueObject(String key, String value) {
			super();
			this.key = key;
			this.value = value;
		}

		public ValueObject() {
			super();
		}

	}

	public List<ValueObject> getValues() {
		return values;
	}

	public void setValues(List<ValueObject> values) {
		this.values = values;
	}

	public GetStatisticsResponse() {
		// TODO Auto-generated constructor stub
	}

	public GetStatisticsResponse(int applicationStatusCode, String applicationStatusResponse,
			List<ValueObject> values) {
		super(applicationStatusCode, applicationStatusResponse);
		this.values = values;
	}

}
