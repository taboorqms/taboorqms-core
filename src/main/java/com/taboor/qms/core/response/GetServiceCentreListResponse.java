package com.taboor.qms.core.response;

import java.util.List;

public class GetServiceCentreListResponse extends GenStatusResponse {

	private List<ServiceCentreResponse> serviceCenterList;

	public class ServiceCentreResponse {

		public ServiceCentreResponse() {
		}

		private Long serviceCenterId;
		private String serviceCenterName;
		private String serviceCenterNameArabic;
		private String country;
		private int numberOfBranches;

		public Long getServiceCenterId() {
			return serviceCenterId;
		}

		public void setServiceCenterId(Long serviceCenterId) {
			this.serviceCenterId = serviceCenterId;
		}

		public String getServiceCenterName() {
			return serviceCenterName;
		}

		public void setServiceCenterName(String serviceCenterName) {
			this.serviceCenterName = serviceCenterName;
		}

		public String getServiceCenterNameArabic() {
			return serviceCenterNameArabic;
		}

		public void setServiceCenterNameArabic(String serviceCenterNameArabic) {
			this.serviceCenterNameArabic = serviceCenterNameArabic;
		}

		public String getCountry() {
			return country;
		}

		public void setCountry(String country) {
			this.country = country;
		}

		public int getNumberOfBranches() {
			return numberOfBranches;
		}

		public void setNumberOfBranches(int numberOfBranches) {
			this.numberOfBranches = numberOfBranches;
		}
	}

	public List<ServiceCentreResponse> getServiceCenterList() {
		return serviceCenterList;
	}

	public void setServiceCenterList(List<ServiceCentreResponse> serviceCenterList) {
		this.serviceCenterList = serviceCenterList;
	}

	public GetServiceCentreListResponse() {
		// TODO Auto-generated constructor stub
	}

	public GetServiceCentreListResponse(int applicationStatusCode, String applicationStatusResponse,
			List<ServiceCentreResponse> serviceCenterList) {
		super(applicationStatusCode, applicationStatusResponse);
		this.serviceCenterList = serviceCenterList;
	}

}
