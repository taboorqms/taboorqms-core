package com.taboor.qms.core.response;

import java.util.Map;

public class GetValuesResposne extends GenStatusResponse {

	private Map<String, Object> values;

	public GetValuesResposne() {
		// TODO Auto-generated constructor stub
	}

	public Map<String, Object> getValues() {
		return values;
	}

	public void setValues(Map<String, Object> values) {
		this.values = values;
	}

	public GetValuesResposne(int applicationStatusCode, String applicationStatusResponse, Map<String, Object> values) {
		super(applicationStatusCode, applicationStatusResponse);
		this.values = values;
	}

}
