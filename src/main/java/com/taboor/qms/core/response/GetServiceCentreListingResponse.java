package com.taboor.qms.core.response;

import java.time.OffsetDateTime;
import java.util.List;

import com.taboor.qms.core.model.ServiceCenter;
import com.taboor.qms.core.response.GenStatusResponse;

public class GetServiceCentreListingResponse extends GenStatusResponse {

	private List<ServiceCenterObject> serviceCenterList;

	public static class ServiceCenterObject {

		private ServiceCenter serviceCenter;
		private int userCount;
		private int branchCount;
		private int issueCount;
		private String planName;
		private String planNameArabic;
		private OffsetDateTime nextPaymentDueDate;

		public ServiceCenter getServiceCenter() {
			return serviceCenter;
		}

		public void setServiceCenter(ServiceCenter serviceCenter) {
			this.serviceCenter = serviceCenter;
		}

		public int getUserCount() {
			return userCount;
		}

		public void setUserCount(int userCount) {
			this.userCount = userCount;
		}

		public int getBranchCount() {
			return branchCount;
		}

		public void setBranchCount(int branchCount) {
			this.branchCount = branchCount;
		}

		public int getIssueCount() {
			return issueCount;
		}

		public void setIssueCount(int issueCount) {
			this.issueCount = issueCount;
		}

		public String getPlanName() {
			return planName;
		}

		public void setPlanName(String planName) {
			this.planName = planName;
		}

		public String getPlanNameArabic() {
			return planNameArabic;
		}

		public void setPlanNameArabic(String planNameArabic) {
			this.planNameArabic = planNameArabic;
		}

		public OffsetDateTime getNextPaymentDueDate() {
			return nextPaymentDueDate;
		}

		public void setNextPaymentDueDate(OffsetDateTime nextPaymentDueDate) {
			this.nextPaymentDueDate = nextPaymentDueDate;
		}

		public ServiceCenterObject() {
			super();
		}

	}

	public List<ServiceCenterObject> getServiceCenterList() {
		return serviceCenterList;
	}

	public void setServiceCenterList(List<ServiceCenterObject> serviceCenterList) {
		this.serviceCenterList = serviceCenterList;
	}

	public GetServiceCentreListingResponse(int applicationStatusCode, String applicationStatusResponse,
			List<ServiceCenterObject> serviceCenterList) {
		super(applicationStatusCode, applicationStatusResponse);
		this.serviceCenterList = serviceCenterList;
	}

}
