package com.taboor.qms.core.response;

public class GenericErrorResponse {

	private int applicationStatusCode;
	private String errorCode;
	private String applicationStatusResponse;
	private String devMessage;

	public int getApplicationStatusCode() {
		return applicationStatusCode;
	}

	public void setApplicationStatusCode(int applicationStatusCode) {
		this.applicationStatusCode = applicationStatusCode;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getApplicationStatusResponse() {
		return applicationStatusResponse;
	}

	public void setApplicationStatusResponse(String applicationStatusResponse) {
		this.applicationStatusResponse = applicationStatusResponse;
	}

	public String getDevMessage() {
		return devMessage;
	}

	public void setDevMessage(String devMessage) {
		this.devMessage = devMessage;
	}

}
