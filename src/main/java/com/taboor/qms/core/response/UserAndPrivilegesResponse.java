package com.taboor.qms.core.response;

import java.util.List;

import com.taboor.qms.core.model.User;
import com.taboor.qms.core.utils.PrivilegeName;

public class UserAndPrivilegesResponse extends GenStatusResponse {

	private User user;
	private List<PrivilegeName> privileges;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<PrivilegeName> getPrivileges() {
		return privileges;
	}

	public void setPrivileges(List<PrivilegeName> privileges) {
		this.privileges = privileges;
	}

	public UserAndPrivilegesResponse() {
		super();
	}

	public UserAndPrivilegesResponse(User user, List<PrivilegeName> privileges) {
		super();
		this.user = user;
		this.privileges = privileges;
	}

}
