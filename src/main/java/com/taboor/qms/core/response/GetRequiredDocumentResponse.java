package com.taboor.qms.core.response;

import java.util.Collection;

import com.taboor.qms.core.model.ServiceRequirement;

public class GetRequiredDocumentResponse extends GenStatusResponse {

	private Collection<ServiceRequirement> requirements;

	public Collection<ServiceRequirement> getRequirements() {
		return requirements;
	}

	public void setRequirements(Collection<ServiceRequirement> requirements) {
		this.requirements = requirements;
	}

	public GetRequiredDocumentResponse() {
		// TODO Auto-generated constructor stub
	}

	public GetRequiredDocumentResponse(int applicationStatusCode, String applicationStatusResponse,
			Collection<ServiceRequirement> requirements) {
		super(applicationStatusCode, applicationStatusResponse);
		this.requirements = requirements;
	}

}
