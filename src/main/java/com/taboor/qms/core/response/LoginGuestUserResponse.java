package com.taboor.qms.core.response;

public class LoginGuestUserResponse extends GenStatusResponse {

	private String sessionToken;
	private String refreshToken;
	private Long ticketId;
	private Long branchId;
	private Long serviceId;

	public String getSessionToken() {
		return sessionToken;
	}

	public void setSessionToken(String sessionToken) {
		this.sessionToken = sessionToken;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public Long getTicketId() {
		return ticketId;
	}

	public void setTicketId(Long ticketId) {
		this.ticketId = ticketId;
	}

	public Long getBranchId() {
		return branchId;
	}

	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}

	public Long getServiceId() {
		return serviceId;
	}

	public void setServiceId(Long serviceId) {
		this.serviceId = serviceId;
	}

	public LoginGuestUserResponse() {
		super();
	}

	public LoginGuestUserResponse(int applicationStatusCode, String applicationStatusResponse, String sessionToken,
			String refreshToken, Long ticketId, Long branchId, Long serviceId) {
		super(applicationStatusCode, applicationStatusResponse);
		this.sessionToken = sessionToken;
		this.refreshToken = refreshToken;
		this.ticketId = ticketId;
		this.branchId = branchId;
		this.serviceId = serviceId;
	}

}
