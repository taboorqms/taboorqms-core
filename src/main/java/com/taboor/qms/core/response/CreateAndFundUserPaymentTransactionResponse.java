package com.taboor.qms.core.response;

import com.taboor.qms.core.model.UserPaymentTransaction;

public class CreateAndFundUserPaymentTransactionResponse extends GenStatusResponse {

	private UserPaymentTransaction userPaymentTransaction;

	public UserPaymentTransaction getUserPaymentTransaction() {
		return userPaymentTransaction;
	}

	public void setUserPaymentTransaction(UserPaymentTransaction userPaymentTransaction) {
		this.userPaymentTransaction = userPaymentTransaction;
	}

	public CreateAndFundUserPaymentTransactionResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CreateAndFundUserPaymentTransactionResponse(int applicationStatusCode, String applicationStatusResponse,
			UserPaymentTransaction userPaymentTransaction) {
		super(applicationStatusCode, applicationStatusResponse);
		this.userPaymentTransaction = userPaymentTransaction;
	}

}
