package com.taboor.qms.core.response;

import java.util.List;

import com.taboor.qms.core.model.TaboorEmployee;
import com.taboor.qms.core.model.UserPrivilege;

public class GetTaboorEmployeeListResponse extends GenStatusResponse {

	private List<TaboorEmployeeObject> employees;
	private long totalPrivilegeCount;

	public static class TaboorEmployeeObject {
		private TaboorEmployee employee;
		private List<UserPrivilege> privileges;
		private Long roleId;

		public TaboorEmployee getEmployee() {
			return employee;
		}

		public void setEmployee(TaboorEmployee employee) {
			this.employee = employee;
		}

		public List<UserPrivilege> getPrivileges() {
			return privileges;
		}

		public void setPrivileges(List<UserPrivilege> privileges) {
			this.privileges = privileges;
		}

		public Long getRoleId() {
			return roleId;
		}

		public void setRoleId(Long roleId) {
			this.roleId = roleId;
		}

		public TaboorEmployeeObject() {
			super();
		}

		public TaboorEmployeeObject(TaboorEmployee employee, List<UserPrivilege> privileges, Long roleId) {
			super();
			this.employee = employee;
			this.privileges = privileges;
			this.roleId = roleId;
		}

	}

	public List<TaboorEmployeeObject> getEmployees() {
		return employees;
	}

	public void setEmployees(List<TaboorEmployeeObject> employees) {
		this.employees = employees;
	}

	public GetTaboorEmployeeListResponse() {
		super();
	}

	public long getTotalPrivilegeCount() {
		return totalPrivilegeCount;
	}

	public void setTotalPrivilegeCount(long totalPrivilegeCount) {
		this.totalPrivilegeCount = totalPrivilegeCount;
	}

	public GetTaboorEmployeeListResponse(int applicationStatusCode, String applicationStatusResponse,
			List<TaboorEmployeeObject> employees, long totalPrivilegeCount) {
		super(applicationStatusCode, applicationStatusResponse);
		this.employees = employees;
		this.totalPrivilegeCount = totalPrivilegeCount;
	}

}
