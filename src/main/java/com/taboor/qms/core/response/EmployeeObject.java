package com.taboor.qms.core.response;

import java.util.List;

import com.taboor.qms.core.model.Branch;
import com.taboor.qms.core.model.ServiceCenterEmployee;
import com.taboor.qms.core.model.UserPrivilege;

public class EmployeeObject {

	private ServiceCenterEmployee employee;
	private List<UserPrivilege> privileges;
	private List<Branch> branchList;
	private Long roleId;

	public ServiceCenterEmployee getEmployee() {
		return employee;
	}

	public void setEmployee(ServiceCenterEmployee employee) {
		this.employee = employee;
	}

	public List<UserPrivilege> getPrivileges() {
		return privileges;
	}

	public void setPrivileges(List<UserPrivilege> privileges) {
		this.privileges = privileges;
	}

	public List<Branch> getBranchList() {
		return branchList;
	}

	public void setBranchList(List<Branch> branchList) {
		this.branchList = branchList;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
}
