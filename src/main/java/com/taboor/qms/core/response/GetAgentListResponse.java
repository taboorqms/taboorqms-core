package com.taboor.qms.core.response;

import java.util.List;

import com.taboor.qms.core.model.Agent;
import com.taboor.qms.core.model.BranchCounter;
import com.taboor.qms.core.model.UserPrivilege;
import java.io.Serializable;

public class GetAgentListResponse extends GenStatusResponse implements Serializable{

	private List<AgentObject> agentList;

	public static class AgentObject implements Serializable{
		private Agent agent;
		private BranchCounter assignedBranchCounter;
		private List<UserPrivilege> assignedPrivileges;
		private Double rating;

		public List<UserPrivilege> getAssignedPrivileges() {
			return assignedPrivileges;
		}

		public void setAssignedPrivileges(List<UserPrivilege> assignedPrivileges) {
			this.assignedPrivileges = assignedPrivileges;
		}

		public Agent getAgent() {
			return agent;
		}

		public void setAgent(Agent agent) {
			this.agent = agent;
		}

		public BranchCounter getAssignedBranchCounter() {
			return assignedBranchCounter;
		}

		public void setAssignedBranchCounter(BranchCounter assignedBranchCounter) {
			this.assignedBranchCounter = assignedBranchCounter;
		}

		public Double getRating() {
			return rating;
		}

		public void setRating(Double rating) {
			this.rating = rating;
		}

	}

	public List<AgentObject> getAgentList() {
		return agentList;
	}

	public void setAgentList(List<AgentObject> agentList) {
		this.agentList = agentList;
	}

	public GetAgentListResponse(int applicationStatusCode, String applicationStatusResponse,
			List<AgentObject> agentList) {
		super(applicationStatusCode, applicationStatusResponse);
		this.agentList = agentList;
	}

	public GetAgentListResponse() {
		super();
	}

}
