package com.taboor.qms.core.response;

import com.taboor.qms.core.model.User;

public class GetUserResponse extends GenStatusResponse {

	private User user;
	private String customerPic;
	
	public String getCustomerPic() {
		return customerPic;
	}

	public void setCustomerPic(String customerPic) {
		this.customerPic = customerPic;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public GetUserResponse() {
		super();
	}

	public GetUserResponse(int applicationStatusCode, String applicationStatusResponse, User user, String customerPic) {
		super(applicationStatusCode, applicationStatusResponse);
		this.user = user;
		this.customerPic = customerPic;
	}

}
