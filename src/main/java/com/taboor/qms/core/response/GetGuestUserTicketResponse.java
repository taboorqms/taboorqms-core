package com.taboor.qms.core.response;

public class GetGuestUserTicketResponse extends GenStatusResponse {

	private String ticketNumber;

	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public GetGuestUserTicketResponse() {
		super();
	}

	public GetGuestUserTicketResponse(int applicationStatusCode, String applicationStatusResponse, String ticketNumber) {
		super(applicationStatusCode, applicationStatusResponse);
		this.ticketNumber = ticketNumber;
	}

}
