package com.taboor.qms.core.response;

import java.util.List;

import com.taboor.qms.core.model.UserFAQ;

public class GetAllUserFAQResponse extends GenStatusResponse {

	List<UserFAQ> userFAQList;

	public List<UserFAQ> getUserFAQList() {
		return userFAQList;
	}

	public void setUserFAQList(List<UserFAQ> userFAQList) {
		this.userFAQList = userFAQList;
	}

	public GetAllUserFAQResponse(int applicationStatusCode, String applicationStatusResponse,
			List<UserFAQ> userFAQList) {
		super(applicationStatusCode, applicationStatusResponse);
		this.userFAQList = userFAQList;
	}

}
