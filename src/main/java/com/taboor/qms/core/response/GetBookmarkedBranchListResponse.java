package com.taboor.qms.core.response;

import java.time.LocalTime;
import java.util.List;

public class GetBookmarkedBranchListResponse extends GenStatusResponse {

	public GetBookmarkedBranchListResponse(List<BookmarkedBranches> branchList, int applicationStatusCode, String applicationStatusResponse) {
		super(applicationStatusCode, applicationStatusResponse);
		this.branchList = branchList;
	}
	
	public GetBookmarkedBranchListResponse(){
		
	}

	private List<BookmarkedBranches> branchList;

	public class BookmarkedBranches {
		
		private Long branchId;
		private String branchName;
		private String phoneNumber;
		private LocalTime averageServiceTime;
		private LocalTime averageWaitingTime;
		private Double rating;
		private String city;
		private String address;
		private double longitude;
		private double latitude;
		private int branchStatus;
		private boolean isBookmarked;
		
		public Long getBranchId() {
			return branchId;
		}
		public void setBranchId(Long branchId) {
			this.branchId = branchId;
		}
		public String getBranchName() {
			return branchName;
		}
		public void setBranchName(String branchName) {
			this.branchName = branchName;
		}
		public String getPhoneNumber() {
			return phoneNumber;
		}
		public void setPhoneNumber(String phoneNumber) {
			this.phoneNumber = phoneNumber;
		}
		public LocalTime getAverageServiceTime() {
			return averageServiceTime;
		}
		public void setAverageServiceTime(LocalTime averageServiceTime) {
			this.averageServiceTime = averageServiceTime;
		}
		public LocalTime getAverageWaitingTime() {
			return averageWaitingTime;
		}
		public void setAverageWaitingTime(LocalTime averageWaitingTime) {
			this.averageWaitingTime = averageWaitingTime;
		}
		public Double getRating() {
			return rating;
		}
		public void setRating(Double rating) {
			this.rating = rating;
		}
		public String getCity() {
			return city;
		}
		public void setCity(String city) {
			this.city = city;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public double getLongitude() {
			return longitude;
		}
		public void setLongitude(double longitude) {
			this.longitude = longitude;
		}
		public double getLatitude() {
			return latitude;
		}
		public void setLatitude(double latitude) {
			this.latitude = latitude;
		}
		public int getBranchStatus() {
			return branchStatus;
		}
		public void setBranchStatus(int branchStatus) {
			this.branchStatus = branchStatus;
		}
		public boolean isBookmarked() {
			return isBookmarked;
		}
		public void setBookmarked(boolean isBookmarked) {
			this.isBookmarked = isBookmarked;
		}

	}

	public List<BookmarkedBranches> getBranchList() {
		return branchList;
	}

	public void setBranchList(List<BookmarkedBranches> branchList) {
		this.branchList = branchList;
	}
}
