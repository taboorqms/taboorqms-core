package com.taboor.qms.core.response;

import java.util.List;

import com.taboor.qms.core.model.Notification;

public class GetAllUserNotificationsResponse extends GenStatusResponse {

	List<Notification> userNotificationList;

	public List<Notification> getUserNotificationList() {
		return userNotificationList;
	}

	public void setUserNotificationList(List<Notification> userNotificationList) {
		this.userNotificationList = userNotificationList;
	}

	public GetAllUserNotificationsResponse(int applicationStatusCode, String applicationStatusResponse,
			List<Notification> userNotificationList) {
		super(applicationStatusCode, applicationStatusResponse);
		this.userNotificationList = userNotificationList;
	}

}
