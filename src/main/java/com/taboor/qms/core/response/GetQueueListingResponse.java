package com.taboor.qms.core.response;

import java.util.List;

import com.taboor.qms.core.model.Queue;

public class GetQueueListingResponse extends GenStatusResponse {

	List<QueueListingObject> queueList;

	public static class QueueListingObject {

		private Queue queue;
		private int noOfTickets;
		private int noOfFastpassTicket;
		private int noOfServingCounters;
		private int noOfInQueueTickets;
		private int noOfTotalCounters;
		private int noOfAgents;
		private int rating;

		public int getRating() {
			return rating;
		}

		public void setRating(int rating) {
			this.rating = rating;
		}

		public Queue getQueue() {
			return queue;
		}

		public void setQueue(Queue queue) {
			this.queue = queue;
		}

		public void incrementNoOfTickets() {
			noOfTickets++;
		}

		public void incrementNoOfFastpassTicket() {
			noOfFastpassTicket++;
		}

		public void incrementNoOfServingCounters() {
			noOfServingCounters++;
		}

		public void incrementNoOfInQueueTickets() {
			noOfInQueueTickets++;
		}

		public void incrementNoOfTotalCounters() {
			noOfTotalCounters++;
		}

		public void incrementNoOfAgents() {
			noOfAgents++;
		}

		public QueueListingObject() {
			noOfTotalCounters = 0;
			noOfInQueueTickets = 0;
			noOfServingCounters = 0;
			noOfFastpassTicket = 0;
			noOfTickets = 0;
			noOfAgents = 0;
		}

		public int getNoOfTickets() {
			return noOfTickets;
		}

		public void setNoOfTickets(int noOfTickets) {
			this.noOfTickets = noOfTickets;
		}

		public int getNoOfFastpassTicket() {
			return noOfFastpassTicket;
		}

		public void setNoOfFastpassTicket(int noOfFastpassTicket) {
			this.noOfFastpassTicket = noOfFastpassTicket;
		}

		public int getNoOfServingCounters() {
			return noOfServingCounters;
		}

		public void setNoOfServingCounters(int noOfServingCounters) {
			this.noOfServingCounters = noOfServingCounters;
		}

		public int getNoOfInQueueTickets() {
			return noOfInQueueTickets;
		}

		public void setNoOfInQueueTickets(int noOfInQueueTickets) {
			this.noOfInQueueTickets = noOfInQueueTickets;
		}

		public int getNoOfTotalCounters() {
			return noOfTotalCounters;
		}

		public void setNoOfTotalCounters(int noOfTotalCounters) {
			this.noOfTotalCounters = noOfTotalCounters;
		}

		public int getNoOfAgents() {
			return noOfAgents;
		}

		public void setNoOfAgents(int noOfAgents) {
			this.noOfAgents = noOfAgents;
		}

	}

	public List<QueueListingObject> getQueueList() {
		return queueList;
	}

	public void setQueueList(List<QueueListingObject> queueList) {
		this.queueList = queueList;
	}

	public GetQueueListingResponse() {
		// TODO Auto-generated constructor stub
	}

	public GetQueueListingResponse(int applicationStatusCode, String applicationStatusResponse,
			List<QueueListingObject> queueList) {
		super(applicationStatusCode, applicationStatusResponse);
		this.queueList = queueList;
	}

}
