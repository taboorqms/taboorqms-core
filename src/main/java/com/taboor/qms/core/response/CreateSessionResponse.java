package com.taboor.qms.core.response;

public class CreateSessionResponse extends GenStatusResponse {

	private String sessionToken;
	private String refreshToken;

	public String getSessionToken() {
		return sessionToken;
	}

	public void setSessionToken(String sessionToken) {
		this.sessionToken = sessionToken;
	}

	public CreateSessionResponse() {
		super();
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public CreateSessionResponse(int applicationStatusCode, String applicationStatusResponse, String sessionToken,
			String refreshToken) {
		super(applicationStatusCode, applicationStatusResponse);
		this.sessionToken = sessionToken;
		this.refreshToken = refreshToken;
	}

}
