package com.taboor.qms.core.response;

import java.util.List;

import com.taboor.qms.core.model.Ticket;

public class GetTicketListResponse extends GenStatusResponse {

	private List<Ticket> ticketList;

	public List<Ticket> getTicketList() {
		return ticketList;
	}

	public void setTicketList(List<Ticket> ticketList) {
		this.ticketList = ticketList;
	}

	public GetTicketListResponse() {
		// TODO Auto-generated constructor stub
	}

	public GetTicketListResponse(int applicationStatusCode, String applicationStatusResponse, List<Ticket> ticketList) {
		super(applicationStatusCode, applicationStatusResponse);
		this.ticketList = ticketList;
	}

}
