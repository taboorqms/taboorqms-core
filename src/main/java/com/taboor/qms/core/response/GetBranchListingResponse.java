package com.taboor.qms.core.response;

import java.util.List;

import com.taboor.qms.core.model.Branch;
import com.taboor.qms.core.model.ServiceTAB;

public class GetBranchListingResponse extends GenStatusResponse {

	List<BranchListingObject> branchList;

	public static class BranchListingObject {

		private Branch branch;
		private int noOfTotalCounters;
		private int noOfAgents;
		private List<ServiceTAB> providedServices;

		public Branch getBranch() {
			return branch;
		}

		public void setBranch(Branch branch) {
			this.branch = branch;
		}

		public int getNoOfTotalCounters() {
			return noOfTotalCounters;
		}

		public void setNoOfTotalCounters(int noOfTotalCounters) {
			this.noOfTotalCounters = noOfTotalCounters;
		}

		public int getNoOfAgents() {
			return noOfAgents;
		}

		public void setNoOfAgents(int noOfAgents) {
			this.noOfAgents = noOfAgents;
		}

		public List<ServiceTAB> getProvidedServices() {
			return providedServices;
		}

		public void setProvidedServices(List<ServiceTAB> providedServices) {
			this.providedServices = providedServices;
		}

		public BranchListingObject() {
			super();
		}

	}

	public GetBranchListingResponse() {
		super();
	}

	public List<BranchListingObject> getBranchList() {
		return branchList;
	}

	public void setBranchList(List<BranchListingObject> branchList) {
		this.branchList = branchList;
	}

	public GetBranchListingResponse(int applicationStatusCode, String applicationStatusResponse,
			List<BranchListingObject> branchList) {
		super(applicationStatusCode, applicationStatusResponse);
		this.branchList = branchList;
	}

}
