package com.taboor.qms.core.response;

import java.util.List;

public class GetIdListResponse extends GenStatusResponse{

	private List<Long> ids;

	public List<Long> getIds() {
		return ids;
	}

	public void setIds(List<Long> ids) {
		this.ids = ids;
	}

	public GetIdListResponse() {
		// TODO Auto-generated constructor stub
	}

}
