package com.taboor.qms.core.response;

import com.taboor.qms.core.model.CustomerSettings;

public class GetCustomerSettingsResponse extends GenStatusResponse {

	private CustomerSettings customerSettings;

	public CustomerSettings getCustomerSettings() {
		return customerSettings;
	}

	public void setCustomerSettings(CustomerSettings customerSettings) {
		this.customerSettings = customerSettings;
	}

	public GetCustomerSettingsResponse() {
		// TODO Auto-generated constructor stub
	}

	public GetCustomerSettingsResponse(int applicationStatusCode, String applicationStatusResponse,
			CustomerSettings customerSettings) {
		super(applicationStatusCode, applicationStatusResponse);
		this.customerSettings = customerSettings;
	}

}
