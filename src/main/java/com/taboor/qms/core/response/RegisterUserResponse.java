package com.taboor.qms.core.response;

import com.taboor.qms.core.model.User;

public class RegisterUserResponse extends GenStatusResponse {

	private User user;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public RegisterUserResponse() {
		super();
	}

	public RegisterUserResponse(int applicationStatusCode, String applicationStatusResponse, User user) {
		super(applicationStatusCode, applicationStatusResponse);
		this.user = user;
	}

}
