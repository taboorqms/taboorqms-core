package com.taboor.qms.core.response;

import java.util.Date;
import java.util.List;

import com.taboor.qms.core.utils.PrivilegeName;

public class LoginUserResponse extends GenStatusResponse {

	private String sessionToken;
	private String refreshToken;
	private String role;
	private List<PrivilegeName> privileges;
	private Date expiredOn;

	public String getSessionToken() {
		return sessionToken;
	}

	public void setSessionToken(String sessionToken) {
		this.sessionToken = sessionToken;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public List<PrivilegeName> getPrivileges() {
		return privileges;
	}

	public void setPrivileges(List<PrivilegeName> privileges) {
		this.privileges = privileges;
	}

	public Date getExpiredOn() {
		return expiredOn;
	}

	public void setExpiredOn(Date expiredOn) {
		this.expiredOn = expiredOn;
	}

	public LoginUserResponse() {
		super();
	}

	public LoginUserResponse(int applicationStatusCode, String applicationStatusResponse, String sessionToken,
			String refreshToken, String role, List<PrivilegeName> privileges, Date expiredOn) {
		super(applicationStatusCode, applicationStatusResponse);
		this.sessionToken = sessionToken;
		this.refreshToken = refreshToken;
		this.role = role;
		this.privileges = privileges;
		this.expiredOn = expiredOn;

	}

}
