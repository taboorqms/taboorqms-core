package com.taboor.qms.core.response;

import java.util.List;

import com.taboor.qms.core.model.UserBankCard;

public class GetUserBankCardResponse extends GenStatusResponse {

	private List<UserBankCard> userBankCardList;

	public List<UserBankCard> getUserBankCardList() {
		return userBankCardList;
	}

	public void setUserBankCardList(List<UserBankCard> userBankCardList) {
		this.userBankCardList = userBankCardList;
	}

	public GetUserBankCardResponse(int applicationStatusCode, String applicationStatusResponse,
			List<UserBankCard> userBankCardList) {
		super(applicationStatusCode, applicationStatusResponse);
		this.userBankCardList = userBankCardList;
	}

}
