package com.taboor.qms.core.response;

import java.util.List;

import com.taboor.qms.core.model.PaymentMethod;

public class GetPaymentMethodResponse extends GenStatusResponse {

	private List<PaymentMethod> paymentMethodList;

	public List<PaymentMethod> getPaymentMethodList() {
		return paymentMethodList;
	}

	public void setPaymentMethodList(List<PaymentMethod> paymentMethodList) {
		this.paymentMethodList = paymentMethodList;
	}

	public GetPaymentMethodResponse(int applicationStatusCode, String applicationStatusResponse,
			List<PaymentMethod> paymentMethodList) {
		super(applicationStatusCode, applicationStatusResponse);
		this.paymentMethodList = paymentMethodList;
	}

}
