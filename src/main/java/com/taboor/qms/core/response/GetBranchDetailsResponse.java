//package com.taboor.qms.core.response;
//
//import java.util.List;
//
//import com.taboor.qms.core.model.Branch;
//import com.taboor.qms.core.model.BranchWorkingHours;
//import com.taboor.qms.core.model.ServiceTAB;
//import com.taboor.qms.core.model.ServiceTABRequiredDocumentMapper;
//
//public class GetBranchDetailsResponse extends GenStatusResponse {
//
//	private Branch branch;
//	private ServiceTAB serviceTAB;
//	private int servicesCount;
//	private List<ServiceTABRequiredDocumentMapper> serviceTABRequiredDocumentMapperList;
//	private List<BranchWorkingHours> branchWorkingHourList;
//
//	public Branch getBranch() {
//		return branch;
//	}
//
//	public void setBranch(Branch branch) {
//		this.branch = branch;
//	}
//
//	public ServiceTAB getServiceTAB() {
//		return serviceTAB;
//	}
//
//	public void setServiceTAB(ServiceTAB serviceTAB) {
//		this.serviceTAB = serviceTAB;
//	}
//
//	public List<ServiceTABRequiredDocumentMapper> getServiceTABRequiredDocumentMapperList() {
//		return serviceTABRequiredDocumentMapperList;
//	}
//
//	public void setServiceTABRequiredDocumentMapperList(
//			List<ServiceTABRequiredDocumentMapper> serviceTABRequiredDocumentMapperList) {
//		this.serviceTABRequiredDocumentMapperList = serviceTABRequiredDocumentMapperList;
//	}
//
//	public int getServicesCount() {
//		return servicesCount;
//	}
//
//	public void setServicesCount(int servicesCount) {
//		this.servicesCount = servicesCount;
//	}
//
//	public List<BranchWorkingHours> getBranchWorkingHourList() {
//		return branchWorkingHourList;
//	}
//
//	public void setBranchWorkingHourList(List<BranchWorkingHours> branchWorkingHourList) {
//		this.branchWorkingHourList = branchWorkingHourList;
//	}
//
//	public GetBranchDetailsResponse() {
//		// TODO Auto-generated constructor stub
//	}
//
//	public GetBranchDetailsResponse(int applicationStatusCode, String applicationStatusResponse, Branch branch,
//			ServiceTAB serviceTAB, List<ServiceTABRequiredDocumentMapper> serviceTABRequiredDocumentMapperList,
//			int servicesCount, List<BranchWorkingHours> branchWorkingHourList) {
//		super(applicationStatusCode, applicationStatusResponse);
//		this.branch = branch;
//		this.serviceTABRequiredDocumentMapperList = serviceTABRequiredDocumentMapperList;
//		this.serviceTAB = serviceTAB;
//		this.servicesCount = servicesCount;
//		this.branchWorkingHourList = branchWorkingHourList;
//	}
//
//}
