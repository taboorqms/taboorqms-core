package com.taboor.qms.core.response;

import java.util.List;

public class GetEmployeeListResponse extends GenStatusResponse {

	private List<EmployeeObject> employees;
	private long totalBranchCount;
	private long totalPrivilegeCount;

	public List<EmployeeObject> getEmployees() {
		return employees;
	}

	public void setEmployees(List<EmployeeObject> employees) {
		this.employees = employees;
	}

	public long getTotalBranchCount() {
		return totalBranchCount;
	}

	public void setTotalBranchCount(long totalBranchCount) {
		this.totalBranchCount = totalBranchCount;
	}

	public long getTotalPrivilegeCount() {
		return totalPrivilegeCount;
	}

	public void setTotalPrivilegeCount(long totalPrivilegeCount) {
		this.totalPrivilegeCount = totalPrivilegeCount;
	}

	public GetEmployeeListResponse() {
		// TODO Auto-generated constructor stub
	}

	public GetEmployeeListResponse(int applicationStatusCode, String applicationStatusResponse,
			List<EmployeeObject> employees, long totalBranchCount, long totalPrivilegeCount) {
		super(applicationStatusCode, applicationStatusResponse);
		this.employees = employees;
		this.totalBranchCount = totalBranchCount;
		this.totalPrivilegeCount = totalPrivilegeCount;
	}

}
