package com.taboor.qms.core.response;

import java.util.List;

import com.taboor.qms.core.model.UserFAQType;

public class GetAllUserFAQTypeResponse extends GenStatusResponse {

	List<UserFAQType> userFAQTypeList;

	public List<UserFAQType> getUserFAQTypeList() {
		return userFAQTypeList;
	}

	public void setUserFAQTypeList(List<UserFAQType> userFAQTypeList) {
		this.userFAQTypeList = userFAQTypeList;
	}

	public GetAllUserFAQTypeResponse(int applicationStatusCode, String applicationStatusResponse,
			List<UserFAQType> userFAQTypeList) {
		super(applicationStatusCode, applicationStatusResponse);
		this.userFAQTypeList = userFAQTypeList;
	}

}
