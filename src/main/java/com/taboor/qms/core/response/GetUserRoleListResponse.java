package com.taboor.qms.core.response;

import java.util.List;

import com.taboor.qms.core.model.UserRole;

public class GetUserRoleListResponse extends GenStatusResponse {

	private List<RoleObject> roleList;
	private int privilegeCount;

	public static class RoleObject {
		private UserRole role;
		private Long noOfUsers;

		public UserRole getRole() {
			return role;
		}

		public void setRole(UserRole role) {
			this.role = role;
		}

		public Long getNoOfUsers() {
			return noOfUsers;
		}

		public void setNoOfUsers(Long noOfUsers) {
			this.noOfUsers = noOfUsers;
		}

		public RoleObject() {
			super();
		}

		public RoleObject(UserRole role, Long noOfUsers) {
			super();
			this.role = role;
			this.noOfUsers = noOfUsers;
		}

	}

	public List<RoleObject> getRoleList() {
		return roleList;
	}

	public void setRoleList(List<RoleObject> roleList) {
		this.roleList = roleList;
	}

	public GetUserRoleListResponse() {
		// TODO Auto-generated constructor stub
	}

	public int getPrivilegeCount() {
		return privilegeCount;
	}

	public void setPrivilegeCount(int privilegeCount) {
		this.privilegeCount = privilegeCount;
	}

	public GetUserRoleListResponse(int applicationStatusCode, String applicationStatusResponse,
			List<RoleObject> roleList, int privilegeCount) {
		super(applicationStatusCode, applicationStatusResponse);
		this.roleList = roleList;
		this.privilegeCount = privilegeCount;

	}

}
