package com.taboor.qms.core.response;

public class GenStatusResponse {

	private int applicationStatusCode = -1;
	private String applicationStatusResponse;

	public int getApplicationStatusCode() {
		return applicationStatusCode;
	}

	public void setApplicationStatusCode(int applicationStatusCode) {
		this.applicationStatusCode = applicationStatusCode;
	}

	public String getApplicationStatusResponse() {
		return applicationStatusResponse;
	}

	public void setApplicationStatusResponse(String applicationStatusResponse) {
		this.applicationStatusResponse = applicationStatusResponse;
	}

	public GenStatusResponse() {
		super();
	}

	public GenStatusResponse(int applicationStatusCode, String applicationStatusResponse) {
		this.applicationStatusCode = applicationStatusCode;
		this.applicationStatusResponse = applicationStatusResponse;
	}

}
