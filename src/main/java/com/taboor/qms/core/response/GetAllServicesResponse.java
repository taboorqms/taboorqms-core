package com.taboor.qms.core.response;

import java.util.List;

import com.taboor.qms.core.model.ServiceTAB;

public class GetAllServicesResponse extends GenStatusResponse {

	private List<ServiceTAB> serviceList;

	public List<ServiceTAB> getServiceList() {
		return serviceList;
	}

	public void setServiceList(List<ServiceTAB> serviceList) {
		this.serviceList = serviceList;
	}

	public GetAllServicesResponse() {
		// TODO Auto-generated constructor stub
	}

	public GetAllServicesResponse(int applicationStatusCode, String applicationStatusResponse,
			List<ServiceTAB> serviceList) {
		super(applicationStatusCode, applicationStatusResponse);
		this.serviceList = serviceList;
	}

}
