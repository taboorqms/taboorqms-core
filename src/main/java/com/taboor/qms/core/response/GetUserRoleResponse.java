package com.taboor.qms.core.response;

import com.taboor.qms.core.model.UserRole;

public class GetUserRoleResponse extends GenStatusResponse {

	private UserRole userRole;

	public UserRole getUserRole() {
		return userRole;
	}

	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}

	public GetUserRoleResponse() {
		super();
	}

	public GetUserRoleResponse(int applicationStatusCode, String applicationStatusResponse, UserRole userRole) {
		super(applicationStatusCode, applicationStatusResponse);
		this.userRole = userRole;
	}

}
