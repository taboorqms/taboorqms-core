/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taboor.qms.core.response;

import java.util.List;

/**
 *
 * @author Awais Waheed - Funavry Technologies
 */
public class GetManyFileDataResponse {
    private List<FDObject> filesData;
    private int applicationStatusCode = -1;

    public GetManyFileDataResponse(List<FDObject> filesData) {
        this.filesData = filesData;
    }

    public GetManyFileDataResponse() {
    }

    public List<FDObject> getFilesData() {
        return filesData;
    }

    public void setFilesData(List<FDObject> filesData) {
        this.filesData = filesData;
    }

    public int getApplicationStatusCode() {
        return applicationStatusCode;
    }

    public void setApplicationStatusCode(int applicationStatusCode) {
        this.applicationStatusCode = applicationStatusCode;
    }
    

    public static class FDObject{
        private String id;
        private String data;

        public FDObject(String id, String data) {
            this.id = id;
            this.data = data;
        }

        public FDObject() {
        }
        
        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getData() {
            return data;
        }

        public void setData(String data) {
            this.data = data;
        }
        
        
    }
}
