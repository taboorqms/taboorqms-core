package com.taboor.qms.core.response;

import java.util.List;

import com.taboor.qms.core.model.PaymentPlan;

public class GetPaymentPlanListResponse extends GenStatusResponse {

	List<PaymentPlanObject> paymentPlanList;

	public static class PaymentPlanObject {
		private PaymentPlan paymentPlan;
		private int noOfSubscribers;

		public PaymentPlan getPaymentPlan() {
			return paymentPlan;
		}

		public void setPaymentPlan(PaymentPlan paymentPlan) {
			this.paymentPlan = paymentPlan;
		}

		public int getNoOfSubscribers() {
			return noOfSubscribers;
		}

		public void setNoOfSubscribers(int noOfSubscribers) {
			this.noOfSubscribers = noOfSubscribers;
		}

		public PaymentPlanObject(PaymentPlan paymentPlan, int noOfSubscribers) {
			super();
			this.paymentPlan = paymentPlan;
			this.noOfSubscribers = noOfSubscribers;
		}

		public PaymentPlanObject() {
			super();
		}

	}

	public List<PaymentPlanObject> getPaymentPlanList() {
		return paymentPlanList;
	}

	public void setPaymentPlanList(List<PaymentPlanObject> paymentPlanList) {
		this.paymentPlanList = paymentPlanList;
	}

	public GetPaymentPlanListResponse() {
		// TODO Auto-generated constructor stub
	}

	public GetPaymentPlanListResponse(int applicationStatusCode, String applicationStatusResponse,
			List<PaymentPlanObject> paymentPlanList) {
		super(applicationStatusCode, applicationStatusResponse);
		this.paymentPlanList = paymentPlanList;
	}

}
