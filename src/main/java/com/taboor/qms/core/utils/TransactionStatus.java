package com.taboor.qms.core.utils;

public enum TransactionStatus {

	PENDING(1), CANCELLED(2), PAID(3);

	private int status;

	TransactionStatus(int status) {
		this.status = status;
	}

	public int getStatus() {
		return status;
	}
}
