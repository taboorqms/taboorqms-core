package com.taboor.qms.core.utils;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

/**
 * This class defines common routines for generating authentication signatures
 * for AWS requests.
 */
public class Signature {

	public static byte[] encrypt(String data, String inputkey) throws Exception {

		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
		final SecretKeySpec secretKey = new SecretKeySpec(inputkey.getBytes(), "AES");
		cipher.init(Cipher.ENCRYPT_MODE, secretKey);
		return Base64.encodeBase64(cipher.doFinal(data.getBytes()));
	}

	public static String decrypt(byte[] data, String inputkey) throws Exception {

		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
		final SecretKeySpec secretKey = new SecretKeySpec(inputkey.getBytes(), "AES");
		cipher.init(Cipher.DECRYPT_MODE, secretKey);
		final String decryptedString = new String(cipher.doFinal(Base64.decodeBase64(data)));
		return decryptedString;

	}
}