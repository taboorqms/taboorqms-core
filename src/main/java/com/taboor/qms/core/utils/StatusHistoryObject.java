package com.taboor.qms.core.utils;

import java.time.OffsetDateTime;

public class StatusHistoryObject {

	private SupportTicketStatus status;
	private String reason;
	private OffsetDateTime dateTime;

	public SupportTicketStatus getStatus() {
		return status;
	}

	public void setStatus(SupportTicketStatus status) {
		this.status = status;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public OffsetDateTime getDateTime() {
		return dateTime;
	}

	public void setDateTime(OffsetDateTime dateTime) {
		this.dateTime = dateTime;
	}

	public StatusHistoryObject(SupportTicketStatus status, String reason, OffsetDateTime dateTime) {
		super();
		this.status = status;
		this.reason = reason;
		this.dateTime = dateTime;
	}

	public StatusHistoryObject() {
		super();
	}

}
