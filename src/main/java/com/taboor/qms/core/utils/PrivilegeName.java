package com.taboor.qms.core.utils;

public enum PrivilegeName {
	Customer_Login, Customer_Actions, Guest_Ticket,

	Branch_Managment,

	Agent_Login, Agent_Serve,

	// Admin Panel
	Admin_Panel_View_Only, Service_Center_Profile_View, Service_Center_Profile_Edit, Support_Ticket_View,
	Support_Ticket_Edit, Branch_View, Branch_Edit, Agent_View, Agent_Edit, Queue_View, Queue_Edit, User_View,
	User_Managment, Service_Center_View, Service_Center_Edit, Invoice_View, Invoice_Edit, Subscription_Plan_View,
	Subscription_Plan_Edit, FAQ_View, FAQ_Edit, Manual_View, Manual_Edit

}
