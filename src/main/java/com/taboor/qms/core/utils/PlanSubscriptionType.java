package com.taboor.qms.core.utils;

public enum PlanSubscriptionType {

	DEMO(0), ANNUALY(1), MONTHLY(2);

	private int status;

	PlanSubscriptionType(int status) {
		this.status = status;
	}

	public int getStatus() {
		return status;
	}
}
