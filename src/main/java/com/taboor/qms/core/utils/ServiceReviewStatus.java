package com.taboor.qms.core.utils;

public enum ServiceReviewStatus {

	SATISFIED(1), AVERAGE(2), POOR(3), OTHER(4);

	private int status;

	ServiceReviewStatus(int status) {
		this.status = status;
	}

	public int getStatus() {
		return status;
	}
}
