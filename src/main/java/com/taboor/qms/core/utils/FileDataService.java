package com.taboor.qms.core.utils;

import java.util.List;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.taboor.qms.core.exception.xyzResponseCode;
import com.taboor.qms.core.payload.GetManyFileDataPayload;
import com.taboor.qms.core.payload.GetManyFileDataPayload.FilePathObject;
import com.taboor.qms.core.response.GetManyFileDataResponse;
import com.taboor.qms.core.response.GetManyFileDataResponse.FDObject;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

@Service
public class FileDataService {

    private com.jcraft.jsch.Session session;
    private Channel channel;
    private ChannelSftp sftp;

    private ChannelSftp getSFTP() {
//		Boolean initiate = true;
//		if (sftp != null)
//			initiate = false;
//		else if (sftp.isConnected())
//			initiate = false;

//		if (initiate) {
        try {
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            JSch jsch = new JSch();
            session = jsch.getSession(TaboorQMSConfigurations.HOST_USERNAME.getValue(),
                    TaboorQMSConfigurations.HOST_IP_ADDRESS.getValue(), 22);
            session.setPassword(TaboorQMSConfigurations.HOST_PASSWORD.getValue());
            session.setConfig(config);
            session.connect();
            System.out.println("SSh Connected");

            channel = session.openChannel("sftp");
            channel.connect();

            sftp = (ChannelSftp) channel;

        } catch (Exception e) {
            e.printStackTrace();
        }
//		}
        return sftp;
    }

    private void closeConnection() {
        if (channel != null) {
            if (channel.isConnected()) {
                channel.disconnect();
            }
        }
        if (session != null) {
            if (session.isConnected()) {
                session.disconnect();
            }
        }
        if (sftp != null) {
            if (sftp.isConnected()) {
                sftp.disconnect();
            }
        }
        System.out.println("SSh Closed");

    }

    public Boolean save(List<FileDataObject> files) {
        Boolean response = false;
        try {
            for (FileDataObject file : files) {
                getSFTP().put(file.getMultipartFile().getInputStream(), file.getFilePath());
            }
            response = true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }
        return response;
    }

    public byte[] fetch(String filePath) {
        byte[] data = new String("").getBytes();
        if (filePath != null)
			try {
            data = IOUtils.toByteArray(getSFTP().get(filePath));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return data;
    }

    public Boolean remove(List<String> filePathList) {
        Boolean response = false;
        try {
            for (String filePath : filePathList) {
                getSFTP().rm(filePath);
            }
            response = true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }
        return response;
    }
    
    public GetManyFileDataResponse fetchMany(GetManyFileDataPayload payload){
        int responseCode;
        List<FDObject> filesData = new ArrayList<>();
        
        List <FilePathObject> filePaths = payload.getFilePaths();
        if(filePaths != null && filePaths.size() > 0){
            filePaths.forEach(fpObj -> {
                filesData.add(new FDObject(fpObj.getId(), 
                        Base64.getEncoder().encodeToString(fetch(new String(Base64.getDecoder().decode(fpObj.getPath().getBytes()))))));
            });
            responseCode = xyzResponseCode.SUCCESS.getCode();
        } else{
            filesData.add(new FDObject("Empty", "Payload is Empty"));
            responseCode = xyzResponseCode.ERROR.getCode();
        }
        GetManyFileDataResponse response = new GetManyFileDataResponse();
        response.setApplicationStatusCode(responseCode);
        response.setFilesData(filesData);
        return response;
    }

}
