package com.taboor.qms.core.utils;

import java.util.List;

public class RSocketPayload {

	private List<NotificationType> types;
	private RSocketNotificationSubject notificationSubject;
	private List<String> information;
	private Long recipientId;

	public List<NotificationType> getTypes() {
		return types;
	}

	public void setTypes(List<NotificationType> types) {
		this.types = types;
	}

	public RSocketNotificationSubject getNotificationSubject() {
		return notificationSubject;
	}

	public void setNotificationSubject(RSocketNotificationSubject notificationSubject) {
		this.notificationSubject = notificationSubject;
	}

	public List<String> getInformation() {
		return information;
	}

	public void setInformation(List<String> information) {
		this.information = information;
	}

	public Long getRecipientId() {
		return recipientId;
	}

	public void setRecipientId(Long recipientId) {
		this.recipientId = recipientId;
	}

	public RSocketPayload() {
		super();
	}

	public RSocketPayload(List<NotificationType> types, RSocketNotificationSubject notificationSubject,
			List<String> information, Long recipientId) {
		super();
		this.types = types;
		this.notificationSubject = notificationSubject;
		this.information = information;
		this.recipientId = recipientId;
	}

}
