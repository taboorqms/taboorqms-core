package com.taboor.qms.core.utils;

public enum InvoiceStatus {
	PAID, UNPAID, OVERDUE, DRAFT, CANCELLED;
}
