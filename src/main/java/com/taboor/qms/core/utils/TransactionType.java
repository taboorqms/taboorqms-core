package com.taboor.qms.core.utils;

public enum TransactionType {

	FASTPASS_PAYMENT(1), FASTPASS_REFUND(2);

	private int status;

	TransactionType(int status) {
		this.status = status;
	}

	public int getStatus() {
		return status;
	}
}
