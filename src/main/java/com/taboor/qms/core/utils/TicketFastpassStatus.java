package com.taboor.qms.core.utils;

public enum TicketFastpassStatus {

	CONFIRMED(1), CANCELLED(2);

	private int status;

	TicketFastpassStatus(int status) {
		this.status = status;
	}

	public int getStatus() {
		return status;
	}
}
