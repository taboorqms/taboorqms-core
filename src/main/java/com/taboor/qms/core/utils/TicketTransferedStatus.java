package com.taboor.qms.core.utils;

public enum TicketTransferedStatus {

	TRANSFERED(1), NOTTRANSFERED(2);

	private int status;

	TicketTransferedStatus(int status) {
		this.status = status;
	}

	public int getStatus() {
		return status;
	}
}
