package com.taboor.qms.core.utils;

public enum TicketType {
	
	FASTPASS(1), STANDARD(2),  STEPOUT(3), CANCELED(4), EXPIRED(5);

	private int status;

	TicketType(int status) {
		this.status = status;
	}

	public int getStatus() {
		return status;
	}
}
