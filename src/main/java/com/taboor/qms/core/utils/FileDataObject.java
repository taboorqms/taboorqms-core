package com.taboor.qms.core.utils;

import org.springframework.web.multipart.MultipartFile;

public class FileDataObject {

	private MultipartFile multipartFile;
	private String filePath;

	public MultipartFile getMultipartFile() {
		return multipartFile;
	}

	public void setMultipartFile(MultipartFile multipartFile) {
		this.multipartFile = multipartFile;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public FileDataObject() {
		super();
	}

	public FileDataObject(MultipartFile multipartFile, String filePath) {
		super();
		this.multipartFile = multipartFile;
		this.filePath = filePath;
	}
}