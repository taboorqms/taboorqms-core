package com.taboor.qms.core.utils;

import org.springframework.http.HttpMethod;

public class MatcherObject {

	private String matcherName;
	private HttpMethod methodType;

	public String getMatcherName() {
		return matcherName;
	}

	public void setMatcherName(String matcherName) {
		this.matcherName = matcherName;
	}

	public HttpMethod getMethodType() {
		return methodType;
	}

	public void setMethodType(HttpMethod methodType) {
		this.methodType = methodType;
	}

	public MatcherObject(String matcherName, HttpMethod methodType) {
		super();
		this.matcherName = matcherName;
		this.methodType = methodType;
	}
}