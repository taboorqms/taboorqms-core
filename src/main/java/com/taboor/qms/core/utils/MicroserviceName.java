package com.taboor.qms.core.utils;

public enum MicroserviceName {
	CUSTOMER(0), ADMIN_PANEL(1), USER_MANAGEMENT(2), QUEUE_MANAGEMENT(3), PAYMENT_MANAGEMENT(4)
	, KCAT_MANAGEMENT(5);

	private int status;

	MicroserviceName(int status) {
		this.status = status;
	}

	public int getStatus() {
		return status;
	}
}
