package com.taboor.qms.core.utils;

public enum TaboorQMSConfigurations {

	PASSWORD_ENCRYPTION_KEY("a4k2baii53hixydt"), GUEST_EMAIL("guestticket@taboor.com"),
	GUEST_PASSWORD("@@guest@@ticket@@"), STRIPE_SECRET_KEY("sk_test_04MhCRFXTfFRmFNcchhUlJuM004RnS6SbG"),
	STRIPE_PUBLIC_KEY("pk_test_u1T1ojPZGtn3yPK8ZgopgBby00pM7mG9q9"), JWT_SECRET_KEY("JWTSuperSecretKeyInTheWorld"),
	JWT_EXPIRATION_MS("1296000000"), RSocket_HOST("190.160.2.4"), RSocket_PORT("7070"),

	HOST_IP_ADDRESS("190.160.2.4"), HOST_USERNAME("zeeshan"), HOST_PASSWORD("taboorqms@1234"),
	HOST_USER_IMAGE_FOLDER("/home/zeeshan/user/pictures/"), HOST_MANUAL_FOLDER("/home/zeeshan/manual/"),
	HOST_SUPPORTTICKET_FOLDER("/home/zeeshan/supportTicket/"),

	SMSGLOBAL_APP_KEY("2fd15629c9bbcaa4f392466baf28670d"), SMSGLOBAL_SECRET_KEY("398f4a047dc9af9a70f9cf8d1954a5e9"),
	SMSGLOBAL_PORT("443"), SMSGLOBAL_BASE_URL("https://api.smsglobal.com/v2"), SMSGLOBAL_SMS_PATH("/sms/");

	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	private TaboorQMSConfigurations(String value) {
		this.value = value;
	}

}
