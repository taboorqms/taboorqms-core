package com.taboor.qms.core.utils;

public enum PushNotification {

	// Customer Application Notifications
	TICKET_BOOKED_1("Ticket ", "Your ticket number is: ", "CTN001"),
	TICKET_SERVING_TURN("Ticket ", "Please proceed to counter: ", "CTN002"),
	TICKET_FASTPASS_BOOKED("Ticket ", "Payment successful, your FastPass ticket number: ", "CTN003"),
	TICKET_CANCEL_1("Ticket ", "Your ticket number: ", "CTN004"),
	TICKET_GET_TIMELINE("Ticket ", "Your ticket has been served", "CTN005"),
	TICKET_GET_QUEUE("Ticket ", "Fetch the tickets queue again", "CTN006"),
	TICKET_BOOKED_2("Ticket ", ". Enjoy your time and your ticket will be called shortly.", "CTN007"),
	TICKET_CANCEL_2("Ticket ", " is cancelled by an Agent. Thank you for using Taboor.", "CTN008"),
	TICKET_CANCEL_3("Ticket ", " is cancelled. Thank you for using Taboor.", "CTN009"),
	TICKET_FASTPASS_BOOKED_2("Ticket ", ". Enjoy your time and your ticket will be called as soon as possible.", "CTN010"),
	TICKET_RECALL_1("Ticket ", "1st Recall- Please proceed to counter:", "CTN002"),
	TICKET_RECALL_2("Ticket ", "2nd Recall- Please proceed to counter:", "CTN002"),
	TICKET_RECALL_3("Ticket ", "Final Recall- Please proceed to counter:", "CTN002"),
	TICKET_TRANSFERRED("Ticket ", "Your ticket is transferred to counter: ", "CTN002"),
	TICKET_FASTPASS_CANCEL_1("Ticket ", "Your FastPass ticket number: ", "CTN004"),
	TICKET_FASTPASS_CANCEL_2("Ticket ", " is cancelled, amount is refunded in Taboor credit", "CTN004"),

	// Counter Application Notifications
	TICKET_FEEDBACK("Ticket ", "Ticket is served, give feedback", "CON001"),
	TICKET_SERVING_COUNTER("Ticket ", "Next ticket turn now", "CON002"),
	CHANGE_AGENT_STATUS("Counter ", "Change the counter status", "CON003"),


	// TV Application Notifications
	CHANGE_COUNTER_TICKET("TV ", "Change counter ticket on tv", "TVN001"),
	NEW_COUNTER_ADDED("TV ", "New counter is added", "TVN002"),

	// Agent Application Notifications
	CHANGE_TICKET_COUNT("Agent App ", "Change count of tickets waiting in queue", "AGN001"),


	;

	private String notificationTitle;
	private String notificationDescription;
	private String notificationCode;

	private PushNotification(String notificationTitle, String notificationDescription, String notificationCode) {
		this.notificationTitle = notificationTitle;
		this.notificationDescription = notificationDescription;
		this.notificationCode = notificationCode;
	}

	public String getNotificationTitle() {
		return notificationTitle;
	}

	public String getNotificationDescription() {
		return notificationDescription;
	}

	public String getNotificationCode() {
		return notificationCode;
	}

}
