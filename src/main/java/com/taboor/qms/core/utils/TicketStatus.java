package com.taboor.qms.core.utils;

public enum TicketStatus {

	BOOKED(1), WAITING(2), SERVING(3), SERVED(4);

	private int status;

	TicketStatus(int status) {
		this.status = status;
	}

	public int getStatus() {
		return status;
	}
}
