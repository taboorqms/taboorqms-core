package com.taboor.qms.core.utils;

public enum ServiceCenterPaymentStatus {

	PAID, PARTIALLY_PAID, TO_BE_PAID, UNPAID
}
