package com.taboor.qms.core.utils;

/**
 * This class defines common routines for generating authentication signatures
 * for AWS requests.
 */
public class EmailTemplates {

	public static String EMAIL_TEMPLATE = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'> <html xmlns='http://www.w3.org/1999/xhtml'> <head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /> <meta name='viewport' content='width=device-width, initial-scale=1.0'/>  <!-- use the font --> <style> body { font-size: 18px; } </style> </head> <body style='margin: 0; padding: 0;'>  <table align='center' border='0' cellpadding='0' cellspacing='0' width='600' style='border-collapse: collapse;'>  <tr> <td bgcolor='#eaeaea' style='padding: 40px 30px 40px 30px;'> <p>Dear NAME,</p> <p>::BODY::</p> <a href = BODY >Verify Email</a> <p>Thanks</p> </td> </tr> <tr> <td bgcolor='#777777' style='padding: 30px 30px 30px 30px;'> <p>TaboorQMS</p> <a href ='http://www.google.com.pk'>Portal Link</a> </td> </tr> </table>  </body> </html>";
	public static String EMAIL_TEMPLATE_SIMPLE = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'> <html xmlns='http://www.w3.org/1999/xhtml'> <head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /> <meta name='viewport' content='width=device-width, initial-scale=1.0'/>  <!-- use the font --> <style> body { font-size: 18px; } </style> </head> <body style='margin: 0; padding: 0;'>  <table align='center' border='0' cellpadding='0' cellspacing='0' width='600' style='border-collapse: collapse;'>  <tr> <td bgcolor='#eaeaea' style='padding: 40px 30px 40px 30px;'> <p>Dear NAME,</p> <p>::BODY::</p> <p>Thanks</p> </td> </tr> <tr> <td bgcolor='#777777' style='padding: 30px 30px 30px 30px;'> <p>TaboorQMS</p> <a href ='http://www.google.com.pk'>Portal Link</a> </td> </tr> </table>  </body> </html>";
	public static String EMAIL_VERIFICATION_LINK = "http://tab-project.info/taboor-qms/usermanagement/confirmemailaddress";
//	public static String PICTURE_SAVE_LINK = "/home/talha/customer/pictures/";
//	
//	public static String CUSTOMER_MS_HOST = "190.160.2.4";
//	public static String TABOOR_USER = "talha";
//	public static String TABOOR_PASSWORD = "taboorqms@1234";

}