package com.taboor.qms.core.utils;

public enum UserVerificationStatus {

	VERIFIED(700), NOTVERIFIED(701);
	
	private int status;

	UserVerificationStatus(int status){
		this.status = status;
	}

	public int getStatus() {
		return status;
	}
}

