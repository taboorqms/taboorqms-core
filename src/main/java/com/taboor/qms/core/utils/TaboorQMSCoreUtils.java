package com.taboor.qms.core.utils;

import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import net.bytebuddy.utility.RandomString;

public class TaboorQMSCoreUtils {

	private static final Logger logger = LoggerFactory.getLogger(TaboorQMSCoreUtils.class);

	public static String generatePasswordTo128Bits() {

		String randomPassword = RandomString.make(10);
		logger.debug("password:**************************************" + randomPassword);
		if (randomPassword.length() == 10) {
			return randomPassword;
		} else if (randomPassword.length() > 10) {
			return randomPassword.substring(0, 10);
		} else {
			int len = 10 - randomPassword.length();
			String s = "";
			for (int i = 0; i < len; i++)
				s += "N";
			return randomPassword + s;
		}
	}

	public static boolean sendEmail(final String from, final String password, String to, String subject, String body) {
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");

		// Get the default Session object.
		Authenticator authenticator = new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(from, password);
			}
		};

		try {
			Session session = Session.getDefaultInstance(props, authenticator);
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from, "Taboor-QMS"));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
			message.setSubject(subject);
			message.setContent(body, "text/html");

			Transport.send(message);
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return true;
	}

	public static String getJwtFromRequest(HttpServletRequest request) {
		String bearerToken = request.getHeader("Authorization");
		if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
			return bearerToken.substring(7, bearerToken.length());
		}
		return null;
	}
}
