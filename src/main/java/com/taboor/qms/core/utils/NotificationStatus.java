package com.taboor.qms.core.utils;

public enum NotificationStatus {

	Sent, Not_Sent, Expired, Error
}
