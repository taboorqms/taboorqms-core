package com.taboor.qms.core.utils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.exception.xyzResponseCode;
import com.taboor.qms.core.response.GenStatusResponse;

public class RestUtil {

	private static final RestTemplate restTemplate = new RestTemplate();

	public static <T> T postSmsGlobal(String url, @Nullable Object request, Class<T> responseType)
			throws TaboorQMSServiceException {

		try {
			ResponseEntity<T> entity = restTemplate.postForEntity(url, request, responseType);
			return entity.getBody();
		} catch (Exception ex) {
			if (ex.getMessage().contains("402"))
				throw new TaboorQMSServiceException(444 + "::" + ex.getMessage());
			throw new TaboorQMSServiceException(444 + "::" + ex.getMessage());
		}
	}

	public static <T> T post(String url, @Nullable Object request, Class<T> responseType)
			throws RestClientException, TaboorQMSServiceException {

		ResponseEntity<T> entity = restTemplate.postForEntity(url, request, responseType);
		if (!entity.getStatusCode().equals(HttpStatus.OK) || entity.getBody() == null)
			throw new TaboorQMSServiceException(444 + "::" + responseType.getName() + " Entity response not exists");
		return entity.getBody();
	}

	public static <T> T get(String url, Class<T> responseType) throws RestClientException, TaboorQMSServiceException {

		ResponseEntity<T> entity = restTemplate.getForEntity(url, responseType);
		if (!entity.getStatusCode().equals(HttpStatus.OK) || entity.getBody() == null)
			throw new TaboorQMSServiceException(444 + "::" + responseType.getName() + " Entity response not exists");
		return entity.getBody();
	}

	public static <T> T postRequest(String url, @Nullable Object request, Class<T> responseType, int exceptionCode,
			String exceptionMessage) throws RestClientException, TaboorQMSServiceException {

		ResponseEntity<T> entity = restTemplate.postForEntity(url, request, responseType);
		if (!entity.getStatusCode().equals(HttpStatus.OK) || entity.getBody() == null)
			throw new TaboorQMSServiceException(exceptionCode + "::" + exceptionMessage);

		try {
			GenStatusResponse response = (GenStatusResponse) entity.getBody();
			if (response.getApplicationStatusCode() == xyzResponseCode.ERROR.getCode())
				throw new TaboorQMSServiceException(
						response.getApplicationStatusCode() + "::" + response.getApplicationStatusResponse());
		} catch (ClassCastException ex) {
		}

		return entity.getBody();
	}

	public static <T> T postRequestNoCheck(String url, @Nullable Object request, Class<T> responseType)
			throws RestClientException, TaboorQMSServiceException {

		ResponseEntity<T> entity = restTemplate.postForEntity(url, request, responseType);
		return entity.getBody();
	}

	public static <T> T postRequestEntity(String url, @Nullable Object request, Class<T> responseType,
			int exceptionCode, String exceptionMessage) throws RestClientException, TaboorQMSServiceException {

		ResponseEntity<T> entity = restTemplate.postForEntity(url, request, responseType);
		if (!entity.getStatusCode().equals(HttpStatus.OK) || entity.getBody() == null)
			throw new TaboorQMSServiceException(exceptionCode + "::" + exceptionMessage);
		return entity.getBody();
	}

	public static <T> T getRequest(String url, Class<T> responseType, int exceptionCode, String exceptionMessage)
			throws RestClientException, TaboorQMSServiceException {

		ResponseEntity<T> entity = restTemplate.getForEntity(url, responseType);
		if (!entity.getStatusCode().equals(HttpStatus.OK) || entity.getBody() == null)
			throw new TaboorQMSServiceException(exceptionCode + "::" + exceptionMessage);
		return entity.getBody();
	}

	public static <T> T getRequestNoCheck(String url, Class<T> responseType)
			throws RestClientException, TaboorQMSServiceException {

		ResponseEntity<T> entity = restTemplate.getForEntity(url, responseType);
		return entity.getBody();
	}

}
