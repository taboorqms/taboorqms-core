package com.taboor.qms.core.utils;

public enum SubscriptionStatus {

	INPROCESS, ACTIVE, INACTIVE
}
