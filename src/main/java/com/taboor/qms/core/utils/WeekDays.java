package com.taboor.qms.core.utils;

public enum WeekDays {

	MONDAY(1), TUESDAY(2), WEDNESDAY(3), THURSDAY(4), FRIDAY(5), SATURDAY(6), SUNDAY(7);

	private int status;

	WeekDays(int status) {
		this.status = status;
	}

	public int getStatus() {
		return status;
	}
}
