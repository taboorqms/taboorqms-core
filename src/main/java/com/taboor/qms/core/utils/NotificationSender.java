package com.taboor.qms.core.utils;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;
import com.taboor.qms.core.exception.TaboorQMSServiceException;

public class NotificationSender {

	private static final Logger logger = LoggerFactory.getLogger(NotificationSender.class);

	public static void sendPushNotification(String title, String description, String deviceToken)
			throws FirebaseMessagingException, TaboorQMSServiceException {

		Message message = Message.builder().setNotification(new Notification(title, description)).setToken(deviceToken)
				.build();

		try {
			FirebaseMessaging.getInstance().send(message);
//			logger.info(
//					"   title-->>" + title + ",   description-->>" + description + ",  deviceToken-->>" + deviceToken);
		} catch (FirebaseMessagingException ex) {
			logger.info("Notification Not Sent On due to '" + ex.getErrorCode() + " : " + ex.getMessage()
					+ "' deviceToken-->>" + deviceToken);
		}
	}

	public static void sendPushNotificationData(String title, String description, String deviceToken,
			Map<String, String> metaData) throws FirebaseMessagingException, TaboorQMSServiceException {

		Message message = Message.builder().setNotification(new Notification(title, description)).setToken(deviceToken)
				.putAllData(metaData).build();

		try {
			FirebaseMessaging.getInstance().send(message);
//			logger.info(
//					"   title-->>" + title + ",   description-->>" + description + ",  deviceToken-->>" + deviceToken);
		} catch (FirebaseMessagingException ex) {
			logger.info("Notification Not Sent On due to '" + ex.getErrorCode() + " : " + ex.getMessage()
					+ "' deviceToken-->>" + deviceToken);
		}
	}

}