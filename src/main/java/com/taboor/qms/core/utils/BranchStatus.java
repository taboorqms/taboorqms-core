package com.taboor.qms.core.utils;

public enum BranchStatus {

	OPEN(1), CLOSED(2);
	
	private int status;

	BranchStatus(int status) {
		this.status = status;
	}

	public int getStatus() {
		return status;
	}
}
