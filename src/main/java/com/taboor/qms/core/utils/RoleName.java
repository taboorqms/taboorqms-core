package com.taboor.qms.core.utils;

public enum RoleName {
	Customer, Taboor_Admin, Service_Center_Admin, Branch_Admin, Agent
}
