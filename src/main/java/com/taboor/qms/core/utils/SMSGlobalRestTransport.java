package com.taboor.qms.core.utils;

import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Collections;
import java.util.Random;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.http.client.utils.URIBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

/**
 * REST Transport
 */
@Service
public class SMSGlobalRestTransport {

	private String key;
	private String secret;
	private String baseUrl;
	private URI uri;
	private String version;
	private String path;
	private int port;

	public SMSGlobalRestTransport() throws URISyntaxException {
		this.key = TaboorQMSConfigurations.SMSGLOBAL_APP_KEY.getValue();
		this.secret = TaboorQMSConfigurations.SMSGLOBAL_SECRET_KEY.getValue();
		this.port = Integer.valueOf(TaboorQMSConfigurations.SMSGLOBAL_PORT.getValue());
		setBaseUrl(TaboorQMSConfigurations.SMSGLOBAL_BASE_URL.getValue());
		path = TaboorQMSConfigurations.SMSGLOBAL_SMS_PATH.getValue();
	}

	public String sendMessage(String origin, String destination, String body) throws Exception {
		long timestamp = System.currentTimeMillis() / 1000L;
		int nonce = new Random().nextInt();
		String mac = getMac("POST", "/sms/", timestamp, nonce);
		String messageXml = toXml(new SMSGlobalMessage(origin, destination, body));

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_XML);
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_XML));
		headers.setBasicAuth(getAuthHeader(mac, timestamp, nonce));

		HttpEntity<String> httpEntity = new HttpEntity<>(messageXml, headers);
		String obj = RestUtil.postSmsGlobal(baseUrl + path, httpEntity, String.class);
		return obj;
	}

	public String toXml(SMSGlobalMessage message) throws JAXBException {
		JAXBContext context = JAXBContext.newInstance(SMSGlobalMessage.class);
		Marshaller marshaller = context.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		StringWriter stringWriter = new StringWriter();
		marshaller.marshal(message, stringWriter);
		return stringWriter.toString();
	}

	public String toJson(SMSGlobalMessage message) {
		Gson gson = new Gson();
		return gson.toJson(message);
	}

	public void extractVersion() {
		String[] paths = uri.getPath().split("/");
		this.version = paths[1];
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) throws URISyntaxException {
		this.version = version;
		URIBuilder builder = new URIBuilder(baseUrl).setPath(version);
		this.baseUrl = builder.toString();
	}

	public String getMac(String httpMethod, String httpPath, long timestamp, int nonce)
			throws NoSuchAlgorithmException, InvalidKeyException {
		Mac mac = Mac.getInstance("HmacSHA256");
		SecretKeySpec secretHash = new SecretKeySpec(secret.getBytes(), "HmacSHA256");
		mac.init(secretHash);
		String message = timestamp + "\n" + nonce + "\n" + httpMethod + "\n/" + version + httpPath + "\n"
				+ uri.getHost() + "\n" + port + "\n\n";
		return Base64.getEncoder().encodeToString((mac.doFinal(message.getBytes())));
	}

	public String getAuthHeader(String mac, long timestamp, int nonce) {
		return "MAC id=\"" + key + "\", ts=\"" + timestamp + "\", nonce=\"" + nonce + "\", mac=\"" + mac + "\"";
	}

	public URI getUri() {
		return uri;
	}

	public void setUri(URI uri) {
		this.uri = uri;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(String baseUrl) throws URISyntaxException {
		this.baseUrl = baseUrl;
		this.uri = new URI(baseUrl);
		extractVersion();
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}
}
