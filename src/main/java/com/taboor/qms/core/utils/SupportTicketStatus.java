package com.taboor.qms.core.utils;

public enum SupportTicketStatus {
	In_Queue, Assigned, Closed
}
