package com.taboor.qms.core.utils;

public enum DeviceName {
	CUSTOMER_APP(0), ADMIN_PANEL_APP(1), KIOSIK_APP(2), TV_APP(3), COUNTER_APP(4)
	, AGENT_APP(5);

	private int code;

	DeviceName(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}
}
