package com.taboor.qms.core.utils;

public enum NotificationType {

	IN_APP(0), EMAIL(1), SMS(2);

	private int type;

	public int getType() {
		return type;
	}

	private NotificationType(int type) {
		this.type = type;
	}

}
