package com.taboor.qms.core.exception;

public class ExceptionType {
	
	public static final int TECHNICAL_ERROR_CODE = -1;
	public static final int LOGICAL_ERROR_CODE = -2;
	public static final int INTEGRATION_ERROR_CODE = -3;
	public static final int VALIDATION_ERROR_CODE = -4;

}
