package com.taboor.qms.core.exception;


public class TaboorQMSDaoException extends TaboorQMSException {

	private static final long serialVersionUID = 1L;
	public TaboorQMSDaoException() {
        super();
    }

    public TaboorQMSDaoException(String message) {
        super(message);
    }

   
    public TaboorQMSDaoException(String message, Throwable cause) {
        super(message, cause);
    }

    
    public TaboorQMSDaoException(Throwable cause) {
        super(cause);
    }
}
