package com.taboor.qms.core.exception;

public class TaboorQMSException extends Exception {

	private static final long serialVersionUID = 1L;
	 	public TaboorQMSException() {
	        super();
	    }

	    public TaboorQMSException(String message) {
	        super(message);
	    }

	   
	    public TaboorQMSException(String message, Throwable cause) {
	        super(message, cause);
	    }

	    
	    public TaboorQMSException(Throwable cause) {
	        super(cause);
	    }
}
