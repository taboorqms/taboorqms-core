package com.taboor.qms.core.exception;

public enum xyzResponseCode {

	SUCCESS(0), ERROR(-1);

	private int code;

	private xyzResponseCode(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

}
