package com.taboor.qms.core.exception;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.taboor.qms.core.response.GenericErrorResponse;
import com.taboor.qms.core.response.StatusCode;

@ControllerAdvice
public class ExceptionHandleAdvice {

	private static final Logger logger = LoggerFactory.getLogger(ExceptionHandleAdvice.class);

	@ExceptionHandler({ Exception.class, TaboorQMSDaoException.class })
	public @ResponseBody GenericErrorResponse handleException(HttpServletRequest request, Exception ex)
			throws TaboorQMSDaoException, Exception {

		GenericErrorResponse genericErrorResponse = new GenericErrorResponse();
		genericErrorResponse.setApplicationStatusCode(-1);

		String message = "";

		if (ex.getMessage() != null)
			message = ex.getMessage();

		if (ex.getCause() != null)
			message = ex.getCause().getMessage();

		String[] tokens = message.split("::");

		if (tokens.length > 1) {
			genericErrorResponse.setErrorCode(tokens[0]);
			genericErrorResponse.setDevMessage(tokens[1]);
			genericErrorResponse.setApplicationStatusResponse(tokens[1]);
			logger.error("Exception: " + genericErrorResponse.getErrorCode() + "-"
					+ genericErrorResponse.getApplicationStatusResponse(), ex.getLocalizedMessage());
			return genericErrorResponse;
		} else {
			genericErrorResponse.setDevMessage(message);
			genericErrorResponse.setApplicationStatusResponse(message);
			if (ex instanceof MethodArgumentNotValidException) {
				genericErrorResponse
						.setErrorCode(ExceptionType.LOGICAL_ERROR_CODE + "" + StatusCode.IN_CORRECT_PARAMETERS);
				genericErrorResponse.setDevMessage(tokens[1]);
				genericErrorResponse.setApplicationStatusResponse(tokens[1]);
			} else {

			}

			logger.error("Exception: " + genericErrorResponse.getErrorCode() + "-"
					+ genericErrorResponse.getApplicationStatusResponse(), ex);
		}
		return genericErrorResponse;
	}
}
