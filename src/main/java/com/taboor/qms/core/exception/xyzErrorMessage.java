package com.taboor.qms.core.exception;

public enum xyzErrorMessage {

	DBCONNECTOR_SERVICE_ERROR(500, "DBConnector Service Error"),
	USERMANAGEMENT_SERVICE_ERROR(501, "User Management Service Error"),
	ACCESS_DENIED(503, "User doesnot have access for this Operation"),
	// Database Exception
	SESSION_ALREADY_EXISTS(1090, "User has already logged in."),
	SESSION_NOT_CREATED(1, "Session not created in Database"),
	SESSION_NOT_UPDATED(2, "Session not updated in Database"),
	SESSION_NOT_DELETED(3, "Session not deleted in Database"),
	USERVERIFICATION_NOT_CREATED(4, "UserVerification not created in Database"),
	USERVERIFICATION_NOT_UPDATED(5, "UserVerification not updated in Database"),
	USERVERIFICATION_NOT_DELETED(6, "UserVerification not deleted in Database"),
	USER_NOT_CREATED(7, "User not created in Database"), USER_NOT_UPDATED(8, "User not updated in Database"),
	USER_NOT_DELETED(9, "User not deleted in Database"),
	USERFEEDBACK_NOT_CREATED(10, "UserFeedback not created in Database"),
	USERFEEDBACK_NOT_UPDATED(11, "UserFeedback not updated in Database"),
	USERFEEDBACK_NOT_DELETED(12, "UserFeedback not deleted in Database"),
	USERFAQ_NOT_CREATED(13, "UserFAQ not created in Database"),
	USERFAQ_NOT_UPDATED(14, "UserFAQ not updated in Database"),
	USERFAQ_NOT_DELETED(15, "UserFAQ not deleted in Database"),
	USERFAQTYPE_NOT_CREATED(16, "UserFAQType not created in Database"),
	USERFAQTYPE_NOT_UPDATED(17, "UserFAQType not updated in Database"),
	USERFAQTYPE_NOT_DELETED(18, "UserFAQType not deleted in Database"),
	USERBANKCARD_NOT_CREATED(19, "UserBankCard not created in Database"),
	USERBANKCARD_NOT_UPDATED(20, "UserBankCard not updated in Database"),
	USERBANKCARD_NOT_DELETED(21, "UserBankCard not deleted in Database"),
	CUSTOMER_NOT_CREATED(22, "Customer not created in Database"),
	CUSTOMER_NOT_UPDATED(23, "Customer not updated in Database"),
	CUSTOMER_NOT_DELETED(24, "Customer not deleted in Database"),
	CUSTOMERSETTING_NOT_CREATED(25, "CustomerSetting not created in Database"),
	CUSTOMERSETTING_NOT_UPDATED(26, "CustomerSetting not updated in Database"),
	CUSTOMERSETTING_NOT_DELETED(27, "CustomerSetting not deleted in Database"),
	BRANCHBOOKMARK_NOT_CREATED(28, "BranchBookmark not created in Database"),
	BRANCHBOOKMARK_NOT_UPDATED(29, "BranchBookmark not updated in Database"),
	BRANCHBOOKMARK_NOT_DELETED(30, "BranchBookmark not deleted in Database"),
	USERPAYMENTTRANSACTION_NOT_CREATED(31, "User Payment Transaction not created in Database"),
	USERPAYMENTTRANSACTION_NOT_UPDATED(32, "User Payment Transaction not updated in Database"),
	USERPAYMENTTRANSACTION_NOT_DELETED(33, "User Payment Transaction not deleted in Database"),
	TICKETUSERMAPPER_NOT_CREATED(34, "Ticket User Mapper not created in Database"),
	TICKETUSERMAPPER_NOT_UPDATED(35, "Ticket User Mapper not updated in Database"),
	TICKETUSERMAPPER_NOT_DELETED(36, "Ticket User Mapper not deleted in Database"),
	TICKETUSERMAPPER_NOT_EXISTS(1030, "Ticket User Mapper not found."),
	QUEUETICKETMAPPER_NOT_CREATED(37, "Queue Ticket Mapper not created in Database"),
	QUEUETICKETMAPPER_NOT_UPDATED(38, "Queue Ticket Mapper not updated in Database"),
	QUEUETICKETMAPPER_NOT_DELETED(39, "Queue Ticket Mapper not deleted in Database"),
	TABOORCONFIGURATION_NOT_CREATED(40, "Taboor Configuration not created in Database"),
	TABOORCONFIGURATION_NOT_UPDATED(41, "Taboor Configuration not updated in Database"),
	TABOORCONFIGURATION_NOT_DELETED(42, "Taboor Configuration not deleted in Database"),
	TICKETFASTPASS_NOT_CREATED(43, "Ticket Fastpass not created in Database"),
	TICKETFASTPASS_NOT_UPDATED(44, "Ticket Fastpass not updated in Database"),
	TICKETFASTPASS_NOT_DELETED(45, "Ticket Fastpass not deleted in Database"),
	TICKET_NOT_CREATED(46, "Ticket not created in Database"), TICKET_NOT_UPDATED(47, "Ticket not updated in Database"),
	TICKET_NOT_DELETED(48, "Ticket not deleted in Database"),
	TICKETSTEPOUT_NOT_CREATED(49, "Ticket Stepout not created in Database"),
	TICKETSTEPOUT_NOT_UPDATED(50, "Ticket Stepout not updated in Database"),
	TICKETSTEPOUT_NOT_DELETED(51, "Ticket Stepout not deleted in Database"),
	TICKETFEEDBACK_NOT_CREATED(52, "Ticket Feedback not created in Database"),
	TICKETFEEDBACK_NOT_UPDATED(53, "Ticket Feedback not updated in Database"),
	TICKETFEEDBACK_NOT_DELETED(54, "Ticket Feedback not deleted in Database"),
	SERVICECENTER_NOT_CREATED(55, "Service Center not created in Database"),
	SERVICECENTER_NOT_UPDATED(56, "Service Center not updated in Database"),
	SERVICECENTER_NOT_DELETED(57, "Service Center not deleted in Database"),
	BRANCHSERVICETABMAPPER_NOT_CREATED(58, "Branch ServiceTAB Mapper not created in Database"),
	BRANCHSERVICETABMAPPER_NOT_UPDATED(59, "Branch ServiceTAB Mapper not updated in Database"),
	BRANCHSERVICETABMAPPER_NOT_DELETED(60, "Branch ServiceTAB Mapper not deleted in Database"),
	BRANCHWORKINGHOURS_NOT_CREATED(61, "Branch Working Hours not created in Database"),
	BRANCHWORKINGHOURS_NOT_UPDATED(62, "Branch Working Hours not updated in Database"),
	BRANCHWORKINGHOURS_NOT_DELETED(63, "Branch Working Hours not deleted in Database"),
	BRANCH_NOT_CREATED(64, "Branch not created in Database"), BRANCH_NOT_UPDATED(65, "Branch not updated in Database"),
	BRANCH_NOT_DELETED(66, "Branch not deleted in Database"),
	BRANCHCOUNTER_NOT_CREATED(67, "Branch Counter not created in Database"),
	BRANCHCOUNTER_NOT_UPDATED(68, "Branch Counter not updated in Database"),
	BRANCHCOUNTER_NOT_DELETED(69, "Branch Counter not deleted in Database"),
	BRANCHCOUNTERSERVICETABMAPPER_NOT_CREATED(70, "Branch Counter Service TAB Mapper not created in Database"),
	BRANCHCOUNTERSERVICETABMAPPER_NOT_UPDATED(71, "Branch Counter Service TAB Mapper not updated in Database"),
	BRANCHCOUNTERSERVICETABMAPPER_NOT_DELETED(72, "Branch Counter Service TAB Mapper not deleted in Database"),
	EMPLOYEEBRANCHMAPPER_NOT_CREATED(73, "Employee Branch Mapper Service TAB Mapper not created in Database"),
	EMPLOYEEBRANCHMAPPER_NOT_UPDATED(74, "Employee Branch Mapper Service TAB Mapper not updated in Database"),
	EMPLOYEEBRANCHMAPPER_NOT_DELETED(75, "Employee Branch Mapper Service TAB Mapper not deleted in Database"),
	SERVICECENTEREMP_NOT_CREATED(76, "Service Center Employee not created in Database"),
	SERVICECENTEREMP_NOT_UPDATED(77, "Service Center Employee not updated in Database"),
	SERVICECENTEREMP_NOT_DELETED(78, "Service Center Employee not deleted in Database"),
	AGENT_NOT_CREATED(79, "Agent not created in Database"), AGENT_NOT_UPDATED(80, "Agent not updated in Database"),
	AGENT_NOT_DELETED(81, "Agent not deleted in Database"),
	SERVICECENTERSUBSCRIPTION_NOT_CREATED(82, "Service Center Subscription not created in Database"),
	SERVICECENTERSUBSCRIPTION_NOT_UPDATED(83, "Service Center Subscription not updated in Database"),
	SERVICECENTERSUBSCRIPTION_NOT_DELETED(84, "Service Center Subscription not deleted in Database"),
	USERPRIVILEGEMAPPER_NOT_CREATED(85, "User Privilege Mapper not created in Database"),
	USERPRIVILEGEMAPPER_NOT_UPDATED(86, "User Privilege Mapper not updated in Database"),
	USERPRIVILEGEMAPPER_NOT_DELETED(87, "User Privilege Mapper not deleted in Database"),
	BRANCHCOUNTERAGENTMAPPER_NOT_CREATED(88, "Branch Counter Agent TAB Mapper not created in Database"),
	BRANCHCOUNTERAGENTMAPPER_NOT_UPDATED(89, "Branch Counter Agent TAB Mapper not updated in Database"),
	BRANCHCOUNTERAGENTMAPPER_NOT_DELETED(90, "Branch Counter Agent TAB Mapper not deleted in Database"),
	SERVICETAB_NOT_CREATED(91, "Service not created in Database"),
	SERVICETAB_NOT_UPDATED(92, "Service not updated in Database"),
	SERVICETAB_NOT_DELETED(93, "Service not deleted in Database"),
	REQUIREDDOCUMENT_NOT_CREATED(94, "Required Document not created in Database"),
	REQUIREDDOCUMENT_NOT_UPDATED(95, "Required Document not updated in Database"),
	REQUIREDDOCUMENT_NOT_DELETED(96, "Required Document not deleted in Database"),
	USERMANUAL_NOT_CREATED(97, "User Manual not created in Database"),
	USERMANUAL_NOT_UPDATED(98, "User Manual not updated in Database"),
	USERMANUAL_NOT_DELETED(99, "User Manual not deleted in Database"),
	SUPPORTTCKET_NOT_CREATED(1001, "Support Ticket not created in Database"),
	SUPPORTTCKET_NOT_UPDATED(1002, "Support Ticket not updated in Database"),
	SUPPORTTCKET_NOT_DELETED(1003, "Support Ticket not deleted in Database"),
	USERROLE_NOT_CREATED(1004, "User Role not created in Database"),
	USERROLE_NOT_UPDATED(1005, "User Role not updated in Database"),
	USERROLE_NOT_DELETED(1006, "User Role not deleted in Database"),
	USERROLE_ALREADY_EXISTS(10061, "Role Name already Exists in the System."),
	PAYMENTPLAN_NOT_CREATED(1007, "Payment Plan not created in Database"),
	PAYMENTPLAN_NOT_UPDATED(1008, "Payment Plan not updated in Database"),
	PAYMENTPLAN_NOT_DELETED(1009, "Payment Plan not deleted in Database"),
	AGENTTICKETCOUNT_NOT_CREATED(1010, "Agent Ticket Count not created in Database"),
	AGENTTICKETCOUNT_NOT_UPDATED(1011, "Agent Ticket Count not updated in Database"),
	AGENTTICKETCOUNT_NOT_DELETED(1012, "Agent Ticket Count not deleted in Database"),
	AGENTTICKETCOUNT_NOT_EXISTS(1013, "Agent Ticket Count not exists"),
	INVOICE_NOT_CREATED(1010, "Invoice not created in Database"),
	INVOICE_NOT_UPDATED(1011, "Invoice not updated in Database"),
	INVOICE_NOT_DELETED(1012, "Invoice not deleted in Database"),
	USERCONFIGURATION_NOT_CREATED(1013, "User Configuration not created in Database"),
	USERCONFIGURATION_NOT_UPDATED(1014, "User Configuration not updated in Database"),
	USERCONFIGURATION_NOT_DELETED(1015, "User Configuration not deleted in Database"),
	USERNOTIFICATIONTYPE_NOT_CREATED(1016, "User Notification Type not created in Database"),
	USERNOTIFICATIONTYPE_NOT_UPDATED(1017, "User Notification Type not updated in Database"),
	USERNOTIFICATIONTYPE_NOT_DELETED(1018, "User Notification Type not deleted in Database"),
	SERVICECENTERCONFIGURATION_NOT_CREATED(1022, "Service Center Configuration not created in Database"),
	SERVICECENTERCONFIGURATION_NOT_UPDATED(1023, "Service Center Configuration not updated in Database"),
	SERVICECENTERCONFIGURATION_NOT_DELETED(1024, "Service Center Configuration not deleted in Database"),
	SERVICECENTERCONFIGURATION_NOT_EXISTS(1025, "Service Center Configuration not exists"),
	TABOOREMP_NOT_CREATED(1026, "Taboor Employee not created in Database"),
	TABOOREMP_NOT_UPDATED(1027, "Taboor Employee not updated in Database"),
	TABOOREMP_NOT_DELETED(1028, "Taboor Employee not deleted in Database"),
	TABOOREMP_NOT_EXIST(1029, "Taboor Employee not Exists in Database"),

	GUESTTICKET_NOT_DELETED(1019, "Guest Ticket not deleted in Database"),
	GUESTTICKET_NOT_CREATED(1020, "Guest Ticket not created in Database"),
	GUESTTICKET_NOT_UPDATED(1021, "Guest Ticket not updated in Database"),

	// Session 101-110
	SESSION_TOKEN_INVALID(101, "Session Token is Invalid"), REFRESH_TOKEN_INVALID(102, "Refresh Token is Invalid"),
	USER_NOT_LOGOUT(103, "User not logout sucessfully."),
	// UserRole 111-120
	USERROLE_NOT_EXISTS(111, "User Role not Exists"),
	USERROLE_ROLE_ACCESS(115, "Not suitable device to login with these credentials"),
	USERROLE_KIOSIK_ACCESS(112, "Only branch admin can login through this device"),
	USERROLE_COUNTER_ACCESS(113, "Only branch admin can login through this device"),
	USERROLE_CUSTOMER_ACCESS(114, "Only customer can login through this device"),
	USERROLE_AGENT_ACCESS(115, "Only agent can login through this device"),

	// User 121-180
	USER_EXISTS(121, "User with this Email already exists in the System"),
	USER_EXISTS_PHONE(121, "User with this Phone Number already exists in the System"),
	USER_NOT_EXISTS(121, "User Not Exists in the System"),
	VERIFICATION_CODE_INVALID(122, "Verification Code is INVALID"),
	VERIFICATION_CODE_EXPIRED(123, "Verification Code is Expired"),
	USERVERIFICATION_OTP_NOT_SENT(124, "Verification Code not Sent"),
	USER_PASSWORD_INCORRECT(125, "User Email or Password is Incorrect"), USERFAQ_TYPE_INVALID(126, "UserFAQ Type is Invalid"),
	USER_PHONE_NOT_VERIFIED(127, "User Phone Number is not Verified"),
	PAYMENT_METHOD_INVALID(128, "Payment Method is Invalid"),CARD_DATE_EXPIRED(135, "Please add a valid bank card."),
	USER_BANK_CARD_NOT_EXISTS(129, "User Bank Card not Exists"),
	USER_BANK_CARD_EXCEPTION_STRIPE(131, "User Bank Card not Exists"),
	USER_NOT_REGISTERED(130, "User not Registered Successfully."), EMAIL_NOT_VERIFIED(134, "User Email not verified"),
	PAYMENT_METHOD_NOT_EXISTS(132, "Payment Method not Exists"),
	PAYMENT_METHOD_NOT_DELTED(133, "Payment Method not deleted in Database"),
        INVALID_CARD_NUMBER(352,"Invalid Card Number"),
        CARD_EXPIRED(353,"Card has Expired"),

	// Customer 181-200
	CUSTOMER_NOT_REGISTERED(181, "Customer not Registered Successfully."),
	TABOOR_PAYMENT_METHOD_INVALID(182, "Taboor Credit Payment Method is Invalid"),
	CUSTOMER_NOT_EXISTS(183, "Customer not Exists"), CUSTOMERSETTING_NOT_EXISTS(184, "Customer Setting not Exists"),

	// Queue 201-270
	BRANCH_NOT_EXISTS(201, "Branch not Exists"), SERVICE_NOT_EXISTS(202, "Service not Exists"),
	BRANCH_SERVICE_NOT_EXISTS(218, "Service donot exists in transfered Branch"),
	QUEUE_NOT_EXISTS(203, "Queue not Exists"), TICKET_IN_QUEUE_NOT_EXISTS(204, "Ticket in Queue not Exists"),
	TICKET_IN_QUEUE_NOT_DELETED(205, "Ticket in Queue not Deleted"),
	TICKET_FASTPASS_NOT_EXISTS(206, "Ticket Fastpass not Exists"),
	TICKET_ALREADY_FASTPASS(207, "Error! Ticket is already Fastpass Ticket"),
	TICKET_ALREADY_STANDARD(208, "Error! Ticket is already Standard Ticket"),
	FASTPASS_TABOORCONFIGURATION_NOT_EXISTS(209, "Fastpass Charges Taboor Configuration not Exists"),
	TICKET_NOT_EXISTS(210, "Ticket not Exists"), TICKET_STEPOUT_USED_ALREADY(210, "Ticket Stepout already Used"),
	TICKET_STEPOUT_NOT_EXISTS(212, "Ticket Stepout not Exists"),
	TICKET_ALREADY_QUEUE(56, "User has already purchased a ticket"),

	// Payment 271-300
	USER_PAYMENT_TRANSACTION_NOT_EXISTS(271, "User Payment Transaction not Exists"),
	USER_PAYMENT_TRANSACTION_EXPIRED(272, "User Payment Transaction is Expired"),
	INSUFFICIENT_BALANCE(273, "Insufficent Balance in Account"),
	USER_PAYMENT_TRANSACTION_NOT_FUNDED(274, "User Payment Transaction not funded"),
	USER_TABOOR_CREDIT_NOT_EXISTS(275, "User Taboor Credit Account not Exists"),
	PAYMENT_PLAN_NOT_EXISTS(276, "Payment Plan Not exists"),

	// AdminPanel 301-400
	SERVICE_SECTOR_NOT_EXISTS(301, "Service Sector Not exists"),
	SERVICE_CENTER_NOT_EXISTS(302, "Service Center Not exists"),
	BRANCH_COUNTER_NOT_EXISTS(303, "Branch Counter Not exists"),
	SERVICE_CENTER_EMPLOYEE_NOT_EXISTS(304, "Service Center Employee Not exists"),
	BRANCH_LIMIT_EXCEEDS(305, "Branch limit exceeds, Kindly Upgrade your Package"),
	SERVICE_LIMIT_EXCEEDS(311, "Service limit exceeds, Kindly Upgrade your Package"),
	AGENT_NOT_EXISTS(306, "Agent Not Exists"), INVOICE_NOT_EXISTS(307, "Invoice Not Exists"),
	BRANCH_USER_LIMIT_EXCEEDS(308, "Branch User limit exceeds"),
	SERVICE_CENTER_INVOICES_EXISTS(309, "Service Center cannot Deleted because still invoices of Service Center Exists"),
	SERVICE_CENTER_BRANCHES_EXISTS(310, "Service Center cannot Deleted because still branches of Service Center Exists"),

	// counter
	COUNTER_NOT_EXISTS(300, "No record of counter"), TICKET_TIMELINE_NOT_EXISTS(301, "No Record of ticket timeline"),
	BRANCH_COUNTER_AGENT_NOT_EXISTS(302, "No Record of branch counter agent"),
	TICKET_SERVED_NOT_CREATED(303, "Ticket Served not created in database"),
	TICKET_STEP_OUT_EXISTS(304, "Customer has already stepped out"),
	TICKET_STEP_OUT_LIMITEXCEEDED(309, "Ticket Step Out Limit exceeded"),
	TICKET_STEP_OUT_NORMALLIMITEXCEEDED(308, "Customer can only step out once"),

	COUNTERCONFIG_NOT_CREATED(315, "Branch Counter Configuration not created in database"),
	COUNTERCONFIG_NOT_EXISTS(316, "Branch Counter Configuration Not exists"),
	COUNTERCONFIG_ALREADY_EXISTS(317, "Branch Counter Configuration Already exists for this counter"),

	AGENTCONFIG_ALREADY_EXISTS(327, "Configuration Already exists for this counter"),
	AGENT_COUNTER_NOT_ASSIGN(328, "Agent is not assigned this counter"),

	BRANCHCOUNTER_CONFIG_NOTEXITS(330, "Counter app configuration not exists for this counter"),

	TVCONFIG_NOT_CREATED(318, "Branch TV Configuration not created in database"),
	TVCONFIG_ALREADY_EXISTS(319, "Branch TV Configuration Already exists for this tv"),
	TVCONFIG_NOT_EXISTS(320, "Branch TV Configuration Not exists"),

	AGENTCOUNTERCONFIG_NOT_CREATED(321, "Agent Counter Configuration not created in database"),
	AGENTCOUNTERCONFIG_NOT_EXISTS(322, "Agent Counter Configuration Not exists"),
	AGENTCOUNTERCONFIG_ALREADY_EXISTS(323, "Agent Counter Configuration Already exists for this counter"),

	// notification
	NOTIFCATION_NOT_CREATED(315, "Notification not created in database"),
	NOTIFCATION_NOT_EXISTS(316, "Notifications of user not exists"),
	NOTIFICATION_NOT_UPDATED(317, "User Notifications not updated in database"),
	FIREBASE_NOTIFICATION_EXCEPTION(318, "Firebase Notifications not sent"),

	// stripe
	TRANSACTION_EXCEPTION_STRIPE(310, "Error during transaction processing"),
	REFUND_EXCEPTION_STRIPE(311, "Error during refund transaction processing"),

	// image
	IMAGE_FORMAT_WRONG(350, "The format of image is not supported"),

	// input stream
	INPUTSTREAM_IS_NULL(351, "The image of customer is not found on server"),
        
//        File system
        FILE_NOT_SAVED(1031, "Error in Saving file"),
        FILE_NOT_DELETED(1032, "Error in deleting file"),
        FILE_IS_EMPTY(1033, "File is empty."),
        
//        Deletion
        SESSION_USER_CANNOT_BE_DELETED(1034, "Current Session User can not be deleted."),
        TABBOR_ADMIN_CANNOT_BE_DELETED(1035, "Taboor Admin can not be deleted."),
        PHONE_EMAIL_INCORRECT(1036, "Email or phone number is not correct."),

//        email
        EMAIL_NOT_SENT(1037, "Unable to send Email")
	;

	private int errorCode;
	private String errorDetails;

	private xyzErrorMessage(int errorCode, String errorDetails) {
		this.errorCode = errorCode;
		this.errorDetails = errorDetails;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public String getErrorDetails() {
		return errorDetails;
	}

}
