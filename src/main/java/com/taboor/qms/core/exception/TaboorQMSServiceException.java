package com.taboor.qms.core.exception;


public class TaboorQMSServiceException extends TaboorQMSException {

	private static final long serialVersionUID = 1L;
	public TaboorQMSServiceException() {
        super();
    }

    public TaboorQMSServiceException(String message) {
        super(message);
    }

   
    public TaboorQMSServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    
    public TaboorQMSServiceException(Throwable cause) {
        super(cause);
    }
}
