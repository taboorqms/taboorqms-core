package com.taboor.qms.core.auth;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestClientException;

import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.exception.xyzErrorMessage;
import com.taboor.qms.core.response.SessionAndPrivilegesResponse;
import com.taboor.qms.core.utils.RestUtil;
import com.taboor.qms.core.utils.TaboorQMSConfigurations;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

@Component
public class JwtTokenProvider {

	private static final Logger logger = LoggerFactory.getLogger(JwtTokenProvider.class);

	private final String jwtSecretKey = TaboorQMSConfigurations.JWT_SECRET_KEY.getValue();

	@Value("${url.microservice.db.connector}")
	private String dbConnectorMicroserviceURL;

	public String generateToken(UserDetails userDetails, Long sessionId) {

		Date expiryDate = new Date(
				new Date().getTime() + Long.valueOf(TaboorQMSConfigurations.JWT_EXPIRATION_MS.getValue()));

		return Jwts.builder().setSubject(userDetails.getUsername()).setId(String.valueOf(sessionId))
				.setIssuedAt(new Date()).setExpiration(expiryDate).signWith(SignatureAlgorithm.HS512, jwtSecretKey)
				.compact();
	}

	public String getUsernameFromJWT(String token) {
		Claims claims = Jwts.parser().setSigningKey(jwtSecretKey).parseClaimsJws(token).getBody();
		return claims.getSubject();
	}

	public Long getExpirationFromJWT(String token) {
		Claims claims = Jwts.parser().setSigningKey(jwtSecretKey).parseClaimsJws(token).getBody();
		return claims.getExpiration().getTime();
	}

	public Long getSessionIdFromJWT(String token) {
		Claims claims = Jwts.parser().setSigningKey(jwtSecretKey).parseClaimsJws(token).getBody();
		return Long.valueOf(claims.getId());
	}

	public ValidateTokenResponse validateToken(String authToken) throws RestClientException, TaboorQMSServiceException {
		ValidateTokenResponse response = new ValidateTokenResponse();

		try {
			Jwts.parser().setSigningKey(jwtSecretKey).parseClaimsJws(authToken);
			Long sessionId = getSessionIdFromJWT(authToken);
			SessionAndPrivilegesResponse sessionAndPrivilegesResponse = RestUtil.get(dbConnectorMicroserviceURL
					+ "/tab/sessions/getActiveSessionAndPrivileges/id?sessionId=" + sessionId,
					SessionAndPrivilegesResponse.class);

			if (sessionAndPrivilegesResponse.getSession() == null)
				response = new ValidateTokenResponse(false, "Session Token is Invalid", null, null);
			else {
				response = new ValidateTokenResponse(true, null, sessionAndPrivilegesResponse.getSession(),
						sessionAndPrivilegesResponse.getPrivileges());
			}
		} catch (RestClientException ex) {
			response.setExceptionString(xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());
		} catch (SignatureException ex) {
			response.setExceptionString("Invalid JWT signature");
		} catch (MalformedJwtException ex) {
			response.setExceptionString("Invalid JWT token");
		} catch (ExpiredJwtException ex) {
			response.setExceptionString("Expired JWT token");
		} catch (UnsupportedJwtException ex) {
			response.setExceptionString("Unsupported JWT token");
		} catch (IllegalArgumentException ex) {
			response.setExceptionString("JWT claims string is empty");
		}
		if (StringUtils.hasText(response.getExceptionString()))
			logger.error(response.getExceptionString());

		return response;
	}
}