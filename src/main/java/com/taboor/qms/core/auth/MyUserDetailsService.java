package com.taboor.qms.core.auth;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.taboor.qms.core.model.Session;
import com.taboor.qms.core.model.UserPrivilege;
import com.taboor.qms.core.utils.PrivilegeName;

@Service
public class MyUserDetailsService implements UserDetailsService {

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return null;
	}

	public UserDetails loadUserBySession(Session session, List<UserPrivilege> privileges) throws Exception {

		List<GrantedAuthority> authorities = privileges.stream()
				.map(entity -> new SimpleGrantedAuthority("ROLE_" + entity.getPrivilegeName().name()))
				.collect(Collectors.toList());

		return new MyUserDetails(session, authorities);
	}

	public UserDetails loadGuestBySession(Session session) throws Exception {

		return new MyUserDetails(session,
				Arrays.asList(new SimpleGrantedAuthority("ROLE_" + PrivilegeName.Guest_Ticket.name())));
	}

	public UserDetails getUserDetailsObject(com.taboor.qms.core.model.User user) throws Exception {
		return new User(user.getEmail(), "", new ArrayList<>());
	}
}