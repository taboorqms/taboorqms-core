package com.taboor.qms.core.auth;

import java.util.List;

import com.taboor.qms.core.model.Session;
import com.taboor.qms.core.model.UserPrivilege;

public class ValidateTokenResponse {

	private Boolean value;
	private String exceptionString;
	private Session session;
	private List<UserPrivilege> privileges;

	public Boolean getValue() {
		return value;
	}

	public void setValue(Boolean value) {
		this.value = value;
	}

	public String getExceptionString() {
		return exceptionString;
	}

	public void setExceptionString(String exceptionString) {
		this.exceptionString = exceptionString;
	}

	public ValidateTokenResponse() {
		this.value = false;
	}

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}

	public List<UserPrivilege> getPrivileges() {
		return privileges;
	}

	public void setPrivileges(List<UserPrivilege> privileges) {
		this.privileges = privileges;
	}

	public ValidateTokenResponse(Boolean value, String exceptionString, Session session,
			List<UserPrivilege> privileges) {
		this.value = value;
		this.exceptionString = exceptionString;
		this.session = session;
		this.privileges = privileges;

	}

}
